const fs = require('fs');

let myCallBackFunction = function(){};

function User(id, login, role, fullname, birthDate, avaUrl, isDisabled) {
this.id = id;
this.login = login;
this.role = role;
this.fullname = fullname;
this.birthDate = birthDate;
this.avaUrl = avaUrl;
this.isDisabled = isDisabled;
}

module.exports = {

getDyId: function(id) {
    let rawdata = fs.readFileSync('models/data.json');  
    let student = JSON.parse(rawdata);
    const Users = student.items;
    return Users.find( x => x.id === id);
},
getAll: function(){
    let rawdata = fs.readFileSync('models/data.json');  
    let student = JSON.parse(rawdata);
    const Users = student.items;
    return Users;
}
}
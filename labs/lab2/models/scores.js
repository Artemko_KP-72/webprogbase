const fs = require('fs');

let myCallBackFunction = function(){};

/*
унікальний ідентифікатор, що присвоюється йому при внесенні у сховище
2 строкових значеня
2 числових значення
1 строку із датою у форматі ISO 8601.
*/

function Score(id, studentId, teacherId, mark, date, comment, type) {
this.id = id;
this.studentId = studentId;
this.teacherId = teacherId;
this.mark = mark;
this.date = date;
this.comment = comment;
this.type = type;
}

/*
    insert(x) - додати у сховище новий елемент та повернути його новий ідентифікатор
    getAll() - отримати списком всі об'єкти зі сховища
    getById(id) - отримати елемент зі сховища за ідентифікатором
    update(x) - оновити дані елемента у сховищі
    delete(id) - видалити елемент зі сховища за ідентифікатором
*/

function read(path) {
    //./models/data
    let rawdata = fs.readFileSync(path); 
    let student = JSON.parse(rawdata);
    return student;
}


function write(elements) {
    var fs = require("fs");
    var fileContent = JSON.stringify(elements, null, 4);
    
    // ./models/

    fs.writeFileSync("models/sample.json", fileContent, (err) => {
        if (err)
            console.error(err);
    })
}

const fruits = [new Score(0, 0, 3, 11, "2018-09-16", "Werry Well", "ClassWork"),
                new Score(1, 0, 4, 7, "2018-09-03", "+", "HomeTask"),
                new Score(2, 2, 3, 9, "2018-09-26", "Cam be better", "Test")]
// write(fruits);
// read('sample.json');

function update (element) {
            let student = read('sample.json');
            console.log(element.id);
            for (let st of student)
                if (st.id === element.id) {
                    student[st.id] = element;
                     console.log(student);
                }
            for (let st of student)
                console.log(st); 
            write(student);
        }

module.exports = {

    deleteScore: function(id) {
        let st = read('models/sample.json');
        st[id] = null;
        write(st);
    },

    insert: function(element) {
        let st = read('models/sample.json');
        st[st.length] = element;
        write(st);
    },

    update: function(element) {
        let student = read('models/sample.json');
        student[element.id] = element;
        write(student);
    },

    getDyId: function(id) {
        let rawdata = fs.readFileSync('models/sample.json');  
        let student = JSON.parse(rawdata);
        return student[id];
    },

    getAll: function(){
        let rawdata = fs.readFileSync('models/sample.json');  
        let student = JSON.parse(rawdata);
        return student;
    }   
}
const User = require("./models/user");
const Scores = require("./models/scores");

const {ServerApp, ConsoleBrowser, InputForm} = require("webprogbase-console-view");

const app = new ServerApp();

const browser = new ConsoleBrowser();

app.use("/", function(req, res) { 
const links = {
    "users" : "Users submenu",
    "edit" : "edit score database"
}
res.send("Hello!", links)
});


// editing menu

/*
insert(x) - додати у сховище новий елемент та повернути його новий ідентифікатор
getAll() - отримати списком всі об'єкти зі сховища
getById(id) - отримати елемент зі сховища за ідентифікатором
update(x) - оновити дані елемента у сховищі
delete(id) - видалити елемент зі сховища за ідентифікатором
*/

// headerrr-r-r-r-r-r
app.use("edit", function(req, res) {
    const links = {
        "insert": "insert score to database",
        "getAll": "show all elements in database",
        "getById": "show element by id",
        "update": "rewrite some data to element",
        "delete": "delete element from dataBase"

    };
        res.send("edit submenu is here", links);
    });

    // insert staff
    app.use("insert", function(req, res) {
        let id = Scores.getAll().length;
        const fields = {
            "id": {
                description: "",
                auto: "" + id,
            },
            "studentId": "Enter studentId",
            "teacherId": "Enter teacherId",
            "mark": "Enter mark",
            "date": "Enter date [for example: 2018-09-29]",
            "comment": "Leave comment",
            "type": "Specify the type"
        };

        const nextState = "display final fields";
        const form = new InputForm(nextState, fields);
            res.send("Please fiil up every field correctly", form);
        });

        app.use("display final fields", function(req, res) {
            
            const Score = req.data;
            const userId = parseInt(req.data.studentId);
            const user = User.getDyId(userId);
            
            if (!isValidStudentId(Score.studentId)) {
                res.send('You`ve entered wrong Student Id: ' + Score.studentId);
                return; 
            }
        
            if (!isValidTeacherId(Score.teacherId)) {
                res.send('You`ve entered wrong Teacher Id: ' + Score.teacherId);
                return; 
            }
        
            if (!isValidMark(Score.mark)) {
                res.send('You`ve entered wrong mark: ' + Score.mark);
                return; 
            }
        
            if (!isValidType(Score.type)) {
                res.send(' You can choose from\n *Exam\n *HomeTask\n *Test\n *ClassWork\nYou`ve entered incorect type: ' + Score.type);
                return; 
            }
            

            const is_valid_date = isValidDate(Score.date);

            if (is_valid_date == null) {
                res.send('You have entered incorect date: ' + Score.date);
                return; 
            } else Score.date = is_valid_date;

            Scores.insert(Score);

            res.send('Now database is renew');
        });


        app.use("getAll", function(req, res) {
    
            const DATA = Scores.getAll();
            let usersListText = "";
            for (let sc of DATA) {
                if (sc != null)
                    usersListText += "* " + sc.id + ") St Id: " + sc.studentId + " Teacher Id: " + sc.teacherId + " Mark: "+ sc.mark + " date: " 
                    + sc.date + " type: " + sc.type + " Comment: "+ sc.comment + '\r\n';
            }
                res.send(usersListText);
            });
        

        app.use("getById", function(req, res) {
            const fields = {
                "id": "Enter score id"
            };
            const nextState = "showScore";
            const form = new InputForm(nextState, fields);
            res.send("Select score!", form);
        });


        app.use("showScore", function(req, res) {
            const ScoreIdStr = req.data.id;
            const ScoreId = parseInt(ScoreIdStr);
            const sc = Scores.getDyId(ScoreId);
            if (sc == null) {
                res.redirect("getById");
                return; 
            };
            res.send("* " + sc.id + ") St Id: " + sc.studentId + " Teacher Id: " + sc.teacherId + "\r\n\t Mark: "+ sc.mark + " date: " 
            + sc.date + "\r\n\t type: " + sc.type + " Comment: "+ sc.comment + '\r\n');
        });

    
        app.use("update", function(req, res) {
            const fields = {
                "id": "Enter score id"
            };
            const nextState = "showExistingScore";
            const form = new InputForm(nextState, fields);
            res.send("updateScore", form);
        });

        app.use("showExistingScore", function(req, res) {
            const ScoresIdStr = req.data.id;
            const ScId = parseInt(ScoresIdStr);
            const sc = Scores.getDyId(ScId);
            if (sc == undefined) {
                res.redirect("update");
                return; 
            };

            const fields = {
                "id": {
                    description: "",
                    auto: "" + ScId,
                },
                "studentId": "Enter studentId",
                "teacherId": "Enter teacherId",
                "mark": "Enter mark",
                "date": "Enter date [for example: 2018-09-29]",
                "comment": "Leave comment",
                "type": "Specify the type"
            };

            const nextState = "reenter values";
            let inf;
            if (sc != null) 
                inf = "* " + sc.id + ") St Id: " + sc.studentId + " Teacher Id: " + sc.teacherId + "\r\n\t Mark: "+ sc.mark + " date: " + sc.date + "\r\n\t type: " + sc.type + " Comment: "+ sc.comment + '\r\n';
            else inf = "null";
            const form = new InputForm(nextState, fields);
            res.send(inf, form);
        });

        app.use("reenter values", function(req, res) {
            
            const Score = req.data;
            
            if (!isValidStudentId(Score.studentId)) {
                res.send('You`ve entered wrong Student Id: ' + Score.studentId);
                return; 
            }
        
            if (!isValidTeacherId(Score.teacherId)) {
                res.send('You`ve entered wrong Teacher Id: ' + Score.teacherId);
                return; 
            }
        
            if (!isValidMark(Score.mark)) {
                res.send('You`ve entered wrong mark: ' + Score.mark);
                return; 
            }
        
            if (!isValidType(Score.type)) {
                res.send(' You can choose from\n *Exam\n *HomeTask\n *Test\n *ClassWork\nYou`ve entered incorect type: ' + Score.type);
                return; 
            }
            

            const is_valid_date = isValidDate(Score.date);

            if (is_valid_date == null) {
                res.send('You have entered incorect date: ' + Score.date);
                return; 
            } else Score.date = is_valid_date;

            Scores.update(Score);

            res.send('Now database is renew');
        });

        app.use("delete", function(req, res) {
            let id = Scores.getAll().length;
            const fields = {
                "id": "enter id value"
            }

            const nextState = "deleteEntity";
            const form = new InputForm(nextState, fields);
            res.send("EntityDeleted", form);

        });

        app.use("deleteEntity", function(req, res) {
            const sc = Scores.getDyId(req.data.id)
            if (sc != null && sc != undefined)
                Scores.deleteScore(req.data.id);
            const nextState = "deleteEntity";
            let inf;
            if (sc == null || sc == undefined)
                inf = "* " + sc.id + ") St Id: " + sc.studentId + " Teacher Id: " + sc.teacherId + "\r\n\t Mark: "+ sc.mark + " date: " 
            + sc.date + "\r\n\t type: " + sc.type + " Comment: "+ sc.comment + '\r\n';
            else inf = "null";
                res.send("This score was deleted forever:\r\n\t" + inf);

        });

// ediding menu end

app.use("users", function(req, res) {
const links = {
    "allUsers": "Show all users",
    "getUser": "Select user"
};
    res.send("User submenu is here", links);
});

app.use("allUsers", function(req, res) {
const users = User.getAll();
let usersListText = "";
for (let user of users) {
    usersListText += "* " + user.id + ") " + user.fullname + '\r\n';
}
    res.send(usersListText);
});

app.use("getUser", function(req, res) {
    const fields = {
        "id": "Enter user id"
    };
    const nextState = "showUser";
    const form = new InputForm(nextState, fields);
        res.send("Select user!", form);
    });

app.use("showUser", function(req, res) {
        const userIdStr = req.data.id;
        const userId = parseInt(userIdStr);
        const user = User.getDyId(userId);
        if (!user) {
            res.redirect("getUser");
            return; 
        }
        res.send('You`ve chosen user with id :' + user.id 
        + '\r\nLogin: ' + user.login + '\r\nFullname: ' + user.fullname);
    })

const PORT = 3030;

app.listen(PORT);

browser.open(PORT);

function isValidStudentId(id) {
    if (User.getDyId(parseInt(id)) == undefined || User.getDyId(parseInt(id)).role == 1)
        return false;
    return true;
}

function isValidTeacherId(id) {
    if (User.getDyId(parseInt(id)) == undefined || User.getDyId(parseInt(id)).role == 0) 
        return false;
    return true; 
}

function isValidMark(mark) {
    if (isNaN(mark) === true || mark < 0 || mark > 12) 
        return false;
    return true; 
}

function isValidType(type) {
    if (!((type === "Exam") || (type === "HomeTask") || (type === "Test") || (type === "ClassWork")))
        return false; 
    return true;
}

function isValidDate(date) {
    if (isNaN(Date.parse(date)))
        return null; 
    return date = (new Date(date)).toString();
}
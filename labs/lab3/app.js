const express = require('express');
const User = require("./models/user.js");
const Scores = require("./models/scores.js");
const fs = require('fs');
const path = require('path');
const mustache = require("mustache-express");

const app = express(); 
const PORT = 5050;
app.listen(PORT); 
//view engine setup 

// will open public/ directory files for http requests
app.use(express.static("public"));
 
app.engine("mst", mustache(path.join(__dirname, "views", "partials"))); 
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "mst");
/*
*/

app.get("/home", function(req, res) { res.render('home'); });


app.get("/user/:id", function(req, res) { 
    const self = User.getDyId(parseInt(req.params.id));

    if (typeof(self) === "undefined")
        res.end(`Sorry, user with id = \"${req.params.id}\" is deleted or does not exist`);
    else {
        const container = {
            self : self,
            studs : onlyStudents(this.self),
            prediction : getRandPrediction()
        };

        res.render('user', container);
    }
});

app.get("/users", function(req, res) { 
    const container = {
        studs : onlyStudents(), 
        teachers : onlyTeachers() 
    };  
    res.render('users', container);
 });

 app.get("/score/:id", function(req, res) { 

    const self = Scores.getDyId(parseInt(req.params.id));

    if (typeof(self) === "undefined")
        res.end(`Sorry, user with id = \"${req.params.id}\" is deleted or does not exist`);
    else {
        const TEACHER = User.getDyId(parseInt(self.teacherId));
        const St = User.getDyId(parseInt(self.studentId));
        console.log(self);
        const del = "This user was deleted from data base";
        const container = {
            Reciver : typeof(St) === "undefined" ? del : St.fullname ,
            id : self.id,
            Date : self.date,
            Mark : self.mark, 
            Type : self.type,
            Subject : typeof(TEACHER) === "undefined" ? del : TEACHER.login,
            Comment : self.comment,
            teacherUrl : typeof(TEACHER) === "undefined" ? del : TEACHER.selfLink,
            teacher : typeof(TEACHER) === "undefined" ? del : TEACHER.fullname
        };

        res.render('scoreInf', container);
    }
});
 
 app.get("/scores", function(req, res) { 
    const container = { 
        scores : getAllValideScores(),
        prediction : getRandPrediction()
    }; 
    res.render('scores', container);
 });
 
app.get("/home", function(req, res) { res.render('home'); });
app.get("/about", function(req, res) { res.render('about'); });
app.get("/sign_in_or_up", function(req, res) { res.send('Sorry, this option is unavailable now, but i am working on it)'); });
app.get("/:path", function(req, res) { res.send(`Sorry path /"${req.params.path}/" is unavailable or does not exist`); });

function getAllValideScores() {

    const init = Scores.getAll();
    let scores = []; 

    for (let score of init)
        if (score !== null && typeof(score) !== "undefined")
            scores.push({
                id : score.id,
                fullname : User.getDyId(parseInt(score.studentId)).fullname,
                mark : score.mark
            });

    return scores;
}

function onlyStudents(self) {
    
    const id = typeof(self) !== "undefined" ? self.id : -1;
    const initial = User.getAll();
    let onlyStudents = [];

    for (let user of initial)
        if (user.id !== id && typeof(user) !== "undefined" && user.role !== 1)
            onlyStudents.push(user);

    return  onlyStudents;
    }

function getRandPrediction() {
    const database = readWebDoc("./data/predictions.txt").split("\n");
    // Return a random number between 1 and 100:
    return database[Math.floor((Math.random() * database.length))];
}

function onlyTeachers(self) {
    const id = typeof(self) !== "undefined" ? self.id : -1;
    const initial = User.getAll();
    let onlyTeachers = [];

    for (let user of initial)
        if (user.id !== id && typeof(user) !== "undefined" && user.role !== 0)
            onlyTeachers.push(user);

    return  onlyTeachers;
}

function readWebDoc(fileName) {
    console.log(__dirname);
    const filePath = path.join(__dirname, fileName);
    return fs.readFileSync(filePath).toString();
};
 
const Scores = require('../models/scores');
const User   = require('../models/user');
const Predictator = require('../predictions/predictions');

const path = require('path');
const fs = require('fs');
// app/routes.js
module.exports = function(app, passport) {

app.get("/user/:userID", (req, res) => {
    const ID = req.params.userID; // string
    console.log(ID.toString());
    console.log("now");
    User.schema.methods.getById(ID)
        .then(currUser => { 
            console.log(currUser);
            let x;
            if (isLoggedIn(req)) {
                x = req.user;
                x.selfLink = "/user/" + ID.toString();
            }
            if (currUser[0])
                res.render("users/user", { 
                    user: x,
                    prediction: Predictator.getWishForUser(req.user === undefined ? undefined : req.user._id),
                    bio: "Lorem ipsum",
                    //studs: getClassmates(id),
                    data: currUser[0]
                });
            else res.status(404).send(`User with id ${ID} not found`);
        })
        .catch(err => res.send(err.toString()))
    });



    app.get("/users", function(req, res) {

    if (!isLoggedIn(req)) {
        res.redirect("/home");
        return;
      }
      
        User.schema.methods.getAll()
            .then(users => {
                if (isLoggedIn(req)) res.render("users/users", {    user : req.user,    users: users});
                else   res.render("users/users", {users: users})
            });
            
     });

     app.get("/deleteAllKEbEnyam", (req, res) => {
        User.schema.methods.delete()
            .then(() => res.send("All Users Succesfully deleted"))
            .catch(err => res.send(err.toString()));
     });

}

function isLoggedIn(req) {
    return req.isAuthenticated();
}

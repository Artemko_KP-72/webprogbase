const User            = require('../models/user');
const Predictator = require('../predictions/predictions');
const fs = require('fs');
// app/routes.js
module.exports = function(app, passport) {


        app.get("/home", function(req, res) { 
            if (isLoggedIn(req)) res.render('menu/home', {user: req.user})
            else res.render('menu/home')
        });

        app.get("/about", function(req, res) { 
            if (isLoggedIn(req)) res.render('menu/about', {user: req.user});
            else res.render('menu/about'); 
        });
        

    
        // =====================================
        // HOME PAGE (with login links) ========
        // =====================================
        app.get('/', function(req, res) {
            if (isLoggedIn(req)) res.render('index', req.user);
            else res.render('index.mst'); // load the index.ejs file
        });
    
        // =====================================
        // LOGIN ===============================
        // =====================================
        // show the login form
        app.get('/login', function(req, res) {
    
            // render the page and pass in any flash data if it exists
            res.render('login.ejs', { message: req.flash('loginMessage') }); 
        });
    
        // process the login form
        // app.post('/login', do all our passport stuff here);
    
        // =====================================
        // SIGNUP ==============================
        // =====================================
        // show the signup form
        app.get('/auth/signup', function(req, res) {
    
            // render the page and pass in any flash data if it exists
            res.render('signup.ejs', { message: req.flash('signupMessage') });
        });
    
        // process the signup form
        // app.post('/signup', do all our passport stuff here);
    
        // =====================================
        // PROFILE SECTION =====================
        // =====================================
        // we will want this protected so you have to be logged in to visit
        // we will use route middleware to verify this (the isLoggedIn function)
        app.get('/profile', (req, res) => {
            if (isLoggedIn(req)) {
                res.render('users/user', {
                    prediction: Predictator.getWishForUser(req.user._id),
                    data: req.user,
                    user : req.user // get the user out of session and pass to template
                });
            }
            else res.send("sorry, you not logged in");
        });
    
        // =====================================
        // LOGOUT ==============================
        // =====================================
        app.get('/auth/logout', function(req, res) {
            req.logout();
            res.redirect('/');
        });

         // process the signup form
    app.post('/auth/signup', (req, res) =>  {

        User.findOne({ 'login' :  req.body.login }, (err, user) => {
            // if there are any errors, return the error before anything else
            if (err) {
                res.send(err.toString() + "err place: app.post('/auth/signup',");
                return;
            }

            // if no user is found, return the message
            if (user)
                res.send("This username Allready taken!");
            else {
                    
                    let fileName = "defaultImage.svg";

                    if (req.files.avatar !== undefined) {
                        const avaFile = req.files.avatar;
                        fileName = req.body.login + avaFile.name.substring(avaFile.name.lastIndexOf("."), avaFile.name.length);
                        const path = "/home/artemko/webprogbase/labs/sesion_training/public/images/" + fileName;
                        fs.writeFileSync(path, avaFile.data);
                    }

                    User.schema.methods.insert( 
                        {
                            login        : req.body.login,
                            password     : User.schema.methods.generateHash(req.body.password).toString(),
                            role         : parseInt(req.body.role) || 0,
                            fullname     : req.body.fullname,
                            email        : req.body.email,
                            birthDate    : req.body.birthDate,
                            avaUrl       : "/images/" + fileName, 
                            selfLink     : "user/",
                            isDisabled   : false,//req.body.isDisabled,
                        })
                        .then(answ => {
                            User.schema.methods.getById(answ._id)
                                .then( newUser => {
                                    res.send(newUser);
                                }) 
                            .catch(err => res.send(err.toString())); 
                        })  //res.send(answ))
                        .catch(err => res.send(err.toString()));
                    }
                });
});

        // process the signup form
    app.get('/getAll', (req, res) =>  {
        User.schema.methods.getAll()
            .then( (data) => {
                console.log(data);
                res.send("Server can now look up on the entities");
            })
            .catch(err => res.send(err.toString));
    });

    app.post('/auth/login', passport.authenticate('local-login', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    };
    
    // route middleware to make sure a user is logged in
    function isLoggedIn(req) {
        return req.isAuthenticated();
    }
module.exports = {
    ServerPort : process.env["PORT"] || 5056,
    DatabaseUrl : process.env['DATABASE_URL'] || 'mongodb://localhost:27017/lab5db'
};
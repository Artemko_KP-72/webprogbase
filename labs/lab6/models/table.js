const mongoose = require('mongoose');


const TableSchema = new mongoose.Schema({
    teacherId: {type: String},
    name: {type: String},
    scores:  {type: Array, default : []},
    students: {type: Array, default : []},
    Subj: {type: String, },
    chat: {type: String}
});

const TableModel = mongoose.model('table', TableSchema);

module.exports = {

    init: (teacherID, name) => {
        return new TableModel({"teacherId": teacherID, "name": name}).save();
    },

    get: (id) => {
        return TableModel.findById(id);
    },

    getAll: () => {
        return TableModel.find();
    },

    delete: (id) => {
        if (id === undefined)
            return TableModel.remove();
        return TableModel.remove( {_id: id});
    },

    update: (id, newArray) => {
        return TableModel.update( 
            { "_id": id },
            { "$set":
               {
                "scores" : newArray 
               }
            });
    },

    addSt: (id, st) => { 
        return TableModel.update( 
            { "_id": id },
            { $addToSet:
               {
                "students" : st,
               }
            });
        },

    addScore: (id, score) => { 
        return TableModel.update( 
            { "_id": id },
            { $addToSet:
                {
                "scores" : score,
                }
            });
        }
};

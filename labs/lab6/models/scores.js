const fs = require('fs');
const mongoose = require('mongoose');
/*
унікальний ідентифікатор, що присвоюється йому при внесенні у сховище
2 строкових значеня
2 числових значення
1 строку із датою у форматі ISO 8601.
*/

const ScoreSchema = new mongoose.Schema({
    // id:     {type: Number, required: true},
    // we will add this after testing ", required: true"
    studentId:  {type: String, required: true},
    teacherId: {type: String, required: true},
    mark: {type: Number, required: true},
    date: {type: Date, required: true},
    comment: {type: String},
    type: {type: String, default: "ClassWork"}
});

const ScoreModel = mongoose.model('Score', ScoreSchema);

// let myCallBackFunction = function(){};

// constructor used for tests
// constructor (){}

function Score(id, studentId, teacherId, mark, date, comment, type) {
        this.id = id;
        this.studentId = studentId;
        this.teacherId = teacherId;
        this.mark = mark;
        this.date = date;
        this.comment = comment;
        this.type = type;
}

/*
    insert(x) - додати у сховище новий елемент та повернути його новий ідентифікатор
    getAll() - отримати списком всі об'єкти зі сховища
    getById(id) - отримати елемент зі сховища за ідентифікатором
    update(x) - оновити дані елемента у сховищі
    delete(id) - видалити елемент зі сховища за ідентифікатором
*/

function read(path) {
    //./models/data
    let rawdata = fs.readFileSync(path); 
    let student = JSON.parse(rawdata);
    return student;
}


function write(elements) {
    let fs = require("fs");
    let fileContent = JSON.stringify(elements, null, 4);
    
    // ./models/

    fs.writeFileSync("models/sample.json", fileContent, (err) => {
        if (err)
            console.error(err);
    })
}

function update (element) {
            let student = read('sample.json');
            console.log(element.id);
            for (let st of student)
                if (st.id === element.id) {
                    student[st.id] = element;
                     console.log(student);
                }
            for (let st of student)
                console.log(st); 
            write(student);
        }

module.exports = {

    delete: (id) => {
        if (id === undefined)
            return ScoreModel.remove();
        return ScoreModel.remove( {_id: id});
    },

    insert: (element) => {
        return new ScoreModel(element).save();
    },

    update: (element) => {
        return ScoreModel.update( 
            { "_id": element.id },
            { "$set":
               {
                "studentId" : element.studentId,
                "teacherId" : element.teacherId,
                "mark" : element.mark,
                "date" : element.date,
                "comment" : element.comment,
                "type" : element.type
               }
            });
        },

    getDyId: (id) => {
        return ScoreModel.findById(id);
    },

    getAll: (parameter) => {
        return parameter === undefined ? ScoreModel.find() : ScoreModel.find(parameter);
    },

    getDyIdAsync: (id, callback) => {
        fs.readFile('models/sample.json', (err, data) => {
            callback(err, JSON.parse(data)[id]);
        });
    },

    getAllAsync: (callback) => {
        fs.readFile('models/sample.json', (err, data) => {
            callback(err, JSON.parse(data));
        });  
    }   
};

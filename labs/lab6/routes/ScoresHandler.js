
const fs = require('fs');
const path = require('path');
const Scores = require('../models/scores');
const Table = require('../models/table');
const express = require('express');
const router = express.Router();
const User = require('../models/user');
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: true}));

router.get("/scores", function(req, res, next) {
    
        
        // const temp = "lol"; // why this string is available in the other function without any common memory container
        Scores.getAll()
            .then(data => {
            User.getAll()
                .then(users => {
                
    
                const inputStr = require('url').parse(req.url, true).query.filter;
                            // console.log(temp); // here we use unavailable block of memory
                // if (err) res.end(err.toString()); 
                // if (error) res.end(error.toString());
    
                let arr = []; 
    
                for (let score of data)
                    if (score !== null && typeof(score) !== "undefined" && isContainKeyWord(score, inputStr,  getUserById(users, score.studentId), getUserById(users, score.teacherId)))
                    // console.log(users[parseInt(score.studentId)]);
                    if (score.studentId && score.teacherId)
                        arr.push({
                                id : score.id,
                                fullname : getUserById(users, score.studentId).fullname, //  users[parseInt()].fullname,
                                mark : score.mark,
                                subj : getUserById(users, score.teacherId).fullname //users[parseInt(score.teacherId)].login
                            });
            
                const standart = 3;
                const scoresPerPage = (typeof(req.params.scoresPerPage) === "undefined") ? standart : parseInt(req.params.scoresPerPage); 
                const page = (typeof(req.query.page) === "undefined") ? 1 : parseInt(req.query.page) ;
                const maxPageCount = getPageSize(arr, scoresPerPage);
    
                const container = {  
                    scores : getScoresList(arr, page, scoresPerPage),
                    prediction : getRandPredictionForScore(), 
                    data : inputStr,
                    prevPage : page > 1 ? (page - 1) : 0,
                    currPage : page, 
                    overall : maxPageCount,    
                    nextPage : page < maxPageCount ? (page + 1) : 0 
                };  
                if (arr.length === 0)
                    res.render("scores/scores", { data : inputStr,    notFound: "Nothing have been found"}); 
                else    res.render('scores/scores', container);
            });
        });
     }); 


     router.get("/score/:id", function(req, res) { 
    
        Scores.getDyId(req.params.id)
            .then(self => { 
    
                console.log(self.studentId + "|||" + self.teacherId);
            if (typeof(self) === null)
                res.end(`Sorry, user with id = \"${req.params.id}\" is deleted or does not exist`);
            else {
                User.getDyId(self.teacherId)
                    .then(TEACHER => {
                    User.getDyId(self.studentId)
                        .then(St => {
                            
                            console.log(St);
                            console.log(TEACHER);
                        const del = "This user was deleted from data base" ;
                        const container = { 
                            Reciver : typeof(St) === "undefined" ? del : St[0].fullname ,
                            ReciverUrl : "/user/0" + St[0]._id,
                            id : self.id,
                            linkToDel : "/deleteScore/" + self.id,
                            linkToUpdate: "/updateForm/" + self.id,
                            Date : self.date,
                            Mark : self.mark, 
                            Type : self.type,
                            Subject : typeof(TEACHER) === "undefined" ? del : TEACHER[0].login,
                            Comment : self.comment,
                            teacherUrl : typeof(TEACHER) === "undefined" ? del : "/user/0" + TEACHER[0].id,
                            teacher : typeof(TEACHER) === "undefined" ? del : TEACHER[0].fullname,
                            _id : self._id
                        };  
    
                        res.render('scores/scoreInf', container);
                    });
                });
            }
        })
        .catch(err => res.send(err.toString()));
    });
 
    
    router.get("/deleteScores", (req, res) => {
        Scores.delete(undefined)
            .then( () => { res.send("All Scores was deleted!")});
            res.send("all scores was deleted");
    });


router.post("/deleteScore/:id", (req, res) => {
    Scores.delete(req.params.id)
        .then((data) => {  res.redirect("/deleteSingleScoreFromTable/findTableIdByYourSelf" + "?score_update_id="  + req.params.id)})///deleteSingleScoreFromTable/:table_id);    }); //
    Scores.getDyId(req.params.id)
        .then((data) => {   console.log(data); });
    
}); 


router.get("/fillScores", (req, res) => {
    Scores.getAllAsync((err, AllScores) => {
        try {
        for (let data of AllScores) {

            console.log((data));
            if (data !== null) {
                const newScore = {
                    id : data.id,
                    studentId : data.studentId,
                    teacherId : data.teacherId,
                    mark : data.mark,
                    date : new Date(data.date),
                    comment : data.comment,
                    type : data.type
                    }; 
    
                Scores.insert(newScore)
                    .then(() => {});
            }
        }
        res.send("All ok");
        //res.redirect("/scores");
    }catch(err) {res.send(err.toString())}
    });
});


router.get("/change_new_mark/:id", (req, res) => {
    const input = require('url').parse(req.url,true).query;

    const element = {
        id: req.params.id,
        studentId : input.studentId, 
        teacherId : input.teacherId,
        mark : input.mark,
        date : new Date(), //new Date(input.date),
        comment : input.comment,
        type : input.type
    } 

    Scores.update(element)
        .then(data => {
            // res.redirect('/tableUpdateScore/' + req.params.id + '?mark=' + element.mark + '&comment=' + element.comment + '&type' + element.type);
            res.redirect('/scores');  
        })
        .catch(err => res.send(err.toString()))
});


router.get("/add_new_mark", function(req, res) {
    User.getAll()
        .then(users => {
            const input = require('url').parse(req.url,true).query;
            // if (input.stud_name) // cheak name or id // implement name first
            let studId = -1;
            let teachId = -1;
            for (let singleUser of users)
                if (typeof(singleUser) !== "undefined")
                    if (singleUser.fullname === input.stud_name)
                        studId = singleUser.id;
                    else if (singleUser.fullname === input.teach_name) teachId = singleUser.id;

            if (studId === -1)  res.end("Sorry, can`t find student " + `${input.stud_name}` + " please, check the correctness of the entered data" );
            if (teachId === -1)  res.end("Sorry, can`t find teacher " + `${input.teach_name}` + " please, check the correctness of the entered data" );
            // there is no need to cheak type becouse in frontend part it`s radio button
            // if (input.type === "Exam" && input.type !== "HomeTask" && input.type !== "Test" && input.type !== "ClassWork")
            if (typeof(input.mark) == "undefined" || isNaN(input.mark) === true || input.mark < 0 || input.mark > 12) res.end("There is no such mark as " + `${input.mark}`);
            const newScore = {
                    id : 0,
                    studentId : studId.toString(),
                    teacherId : teachId.toString(),
                    mark : input.mark.toString(),
                    date : new Date(),
                    comment : input.comment,
                    type : input.type
                    }; 
    
            Scores.insert(newScore)
                .then(el => {
                    Table.getAll()
                    .then(data => {

                        let theOne = {};
                        for (let singleTable of data) {
                            console.log("x");
                            if (el.teacherId.toString() === singleTable.teacherId.toString())
                                theOne = singleTable;
                        }
                        console.log(theOne);
                        if (theOne._id === undefined) res.send("This teacher has no table, please create table first");
                        else res.redirect("/tableAddScore/" + theOne._id.toString() + "?score_id=" + el.id);
                    
                    })
                    .catch(err => {res.send(err.toString())})
                })
                .catch(err => res.send(err.toString()));
        });
    });

function isContainKeyWord(score, inputStr, student, teacher) {
    try {
        if (inputStr === undefined 
            || score.date.toString().includes(inputStr)
            || score.comment.toString().includes(inputStr)
            || score.type.includes(inputStr)
            || score.mark.toString().includes(inputStr) 
            || student.fullname.includes(inputStr) 
            || student.birthDate.toString().includes(inputStr)
            || teacher.login.includes(inputStr)
            || teacher.fullname.includes(inputStr)) return true;
    }catch(err) {console.log(err); console.log("WARNING!!!!"); console.log(score); return false;}
    return false;
}

function findTableByScore(scoreID) {

}


function getUserById(AllUser, id) {
    for (let user of AllUser)
        if (user.id === id)
            return user;
    return {};
}


function getPageSize(arr, scoresPerPage) {
    return Math.ceil( arr.length / scoresPerPage );
} 

function getScoresList(arr, page, scoresPerPage) {  
    const res = [];  
    --page;  
 
    for (let i = page * scoresPerPage; i < page * scoresPerPage + scoresPerPage && i < arr.length; i++)
        res.push(arr[i]); 
    return res;
}

function getRandPredictionForScore() {
    const database = readWebDoc("../data/predictions.txt").split("\n");
    return database[Math.floor((Math.random() * database.length))];
}

function readWebDoc(fileName) {
    console.log(__dirname);
    const filePath = path.join(__dirname, fileName);
    return fs.readFileSync(filePath).toString();
};

module.exports = router;
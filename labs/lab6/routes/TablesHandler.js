const express = require('express');
const Scores = require('../models/scores');
const Table = require('../models/table');
const User = require('../models/user');
const bodyParser = require('body-parser');

const router = express.Router();

router.use(bodyParser.urlencoded({extended: true}));

router.get("/tables", (req, res) => {
    Table.getAll()
        .then(tablesArray => {
            User.getAll()
                .then((usersArray) => {
                    let container = {
                        list : [] 
                    }

                    for (let i = 0; i < tablesArray.length; i++) {
                        const obj = { 
                            linkToSingleUnit : "",
                            table : {},
                            TeacherName : "",
                            Subj: "",
                            name: ""
                        }
                        for (let singleUser of usersArray) {
                            if ((singleUser._id).toString() === tablesArray[i].teacherId) { 
                                // console.log(container.list[i]);
                                // console.log(singleUser.fullname);
                                obj.TeacherName = singleUser.fullname; // adding personal information about teacher
                                obj.Subj = singleUser.login;
                            }
                        }
                        obj.name = tablesArray[i].name; // adding information about table
                        obj.table = tablesArray[i];
                        obj.linkToSingleUnit = "/table/" + tablesArray[i]._id.toString();
                        container.list.push(obj);
                    }
                    res.render("./table/tables", container);
            })
            .catch(err => res.send(err.toString()))
        })
        .catch(err => res.send(err.toString()))
    
});

router.get("/deleteAllTables", (req, res) => {
    Table.delete()
        .then(tables => {
            console.log(tables);
            res.render("./home");
        })
});

router.get("/deleteSingleTable/:id", (req, res) => {
    Table.delete(req.params.id)
        .then((data) => {   res.redirect("/tables");    });
});

router.get("/createNewTable", (req, res) => {
    res.render("./table/newTableForm");
});

router.get("/newTable", (req, res) => {
    const input = require('url').parse(req.url,true).query;
    console.log(input);

    Table.init(input.teacherId, input.name)
        .then(el => {
            console.log(el);
            res.redirect("/table/" + el._id);
        })
        .catch(err => res.send(err.toString()));
});

router.get("/table/:ID", (req, res) => {
    const input = require('url').parse(req.url,true).query;
    Table.get(req.params.ID)
        .then(el => {
            User.getDyId(el.teacherId)
                .then( teacher => {
                    const tableBody = createComplicateStructure(el.students, el.scores);
                    const container = {

                        linkToUpdateScore: "/deleteSingleScoreFromTable/" + req.params.ID,
                        linkToDeleteScore: "/deleteSingleTable/" +  req.params.ID,
                        linkToAddScore: "/tableAddScore/" + req.params.ID,
                        TeacherName : teacher[0].fullname,
                        Subj : teacher[0].login, // todo
                        name : el.name,
                        tableBody // why this is work
                }
                res.render("./table/table", container);
        })
        .catch(err => res.send(err.toString()));
        })
        .catch(err => res.send(err.toString()));
});

router.get("/deleteSingleScoreFromTable/:table_id", (req, res) => {
    console.log("Hello");
    const input = require('url').parse(req.url,true).query;

    if (req.params.table_id === "findTableIdByYourSelf") {
        console.log("Heyya");
        Table.getAll()
        .then( data => {
            for (let singleTable of data) {
                for (let score of singleTable.scores) {
                    console.log(singleTable.teacherId);
                    console.log(score.teacherId);
                    if (score != null && score._id.toString() === input.score_update_id) {
                        console.log("!!!!!!!!!!@@@@@@@@@@@@@!!!!!!!!!!!!!");
                        renewScoreDataBase(req, res, singleTable._id.toString(), input.score_update_id);
                        return;
                    }
                }
            }
            res.redirect("/scores");
        })
        .catch(err => res.send(err.toString()));
    } else renewScoreDataBase(req, res, req.params.table_id, input.score_update_id);
});

router.get('/tableUpdateScore/:id?mark=', (req, res) => { // '/tableUpdateScore/:id?mark=' + element.mark + '&comment=' + element.comment + '&type' + element.type
    const input = require('url').parse(req.url,true).query;
    console.log(req.params.id);
    console.log(input);
    Table.getAll()
    .then( data => {
        for (let singleTable of data) {
            for (let i = 0; i < singleTable.scores.length; i++) {
                if (singleTable.scores[i]._id.toString() === req.params.id ) {
                    console.log("////////////////////////////////////////////////////");
                }
            }
        }
    });
});

function renewScoreDataBase(req, res, table_id, score_id) {
    // score_update_id
    Table.get(table_id)
        .then( data => {

            if(data === null) {res.send(`Sorry the table missing or does not exist`); return;}
            const newArray = [];
            console.log(data);
            for (let singleScore of data.scores)
                if (singleScore._id.toString() != score_id)
                    newArray.push(singleScore);

            Table.update(table_id, newArray) 
                .then( (obj) => {   res.redirect("/table/" + data._id);   })
                .catch(err => {console.log(err.toString());  res.send(err.toString());});
        })
        .catch(err => {console.log(err.toString());  res.send(err.toString());});
}

router.get("/tableAddSt/:table_ID/:stud_ID", (req, res) => { // /tableAddSt/5bdb60e49589142e7847f9d0
    User.getDyId(req.params.stud_ID)
        .then( (user) => {
            Table.addSt(req.params.table_ID, user)
                .then((st) => {console.log(st); 
                    res.redirect("/tables");
                })
                .catch(err => {console.log(err.toString());  res.send(err.toString());});
        })
        .catch(err => {console.log(err.toString());  res.send(err.toString());});
});

//@TODO all cheaks
router.get("/tableAddScore/:table_id", (req, res) => {  
    const input = require('url').parse(req.url,true).query;
    console.log("<>");
    console.log(input);

    Scores.getDyId(input.score_id)
        .then(el => {
            User.getDyId(el.studentId)
                .then( (st) => {
                    console.log(el);
                    console.log(st[0]);
                    Table.get(req.params.table_id)
                        .then(table => {  
                            
                            if (el.teacherId !== table.teacherId) {res.send(`Sorry, this teacher can not post mark on this discipline `); return;}

                            if (!tableAllreadyHaveStudent(table, st[0])) {
                                res.redirect("/tableAddSt/" + table._id + "/" + st[0]._id); // can`t pass the student entity !!!
                                Table.addScore(req.params.table_id, el)
                                    .then(data => {})
                                    .catch(err => {console.log("cant add score to database")});
                            } else {
                                // if (el.teacherId) // author should be the leader of the course
                                Table.addScore(req.params.table_id, el)
                                    .then(data => {
                                        console.log(data);
                                        res.redirect("/tables")})
                                    .catch(err => res.send(err.toString()));
                            }
                            })})
                            .catch(err => {console.log(err.toString()); res.send(err.toString());})
                .catch(err => {console.log(err.toString()); res.send(err.toString());})
        })
        .catch(err => res.send(err.toString()));
});

function createComplicateStructure(studArray, scoreArray) {
    const tableBodyContainer = [];
    const complStrct = {
        self : {},
        scores: []
    }

    for (let singleStud of studArray) {
        const complStrct = {
            self : singleStud,
            scores: []
        }

        for (let singleScore of scoreArray) 
            if (singleStud._id.toString() === singleScore.studentId.toString())
                complStrct.scores.push(singleScore);
        
        tableBodyContainer.push(complStrct);
    }
    return tableBodyContainer;
}

function tableAllreadyHaveStudent(table, st) {
    for (let i of table.students)
        if (i._id.toString() === st._id.toString())
            return true;
    return false;
}

module.exports = router;
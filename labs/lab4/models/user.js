const fs = require('fs');


class User {

// let myCallBackFunction = function(){};

// constructor used for tests
// constructor (){}

constructor (id, login, role, fullname, birthDate, avaUrl, isDisabled) {
this.id = id;
this.login = login;
this.role = role;
this.fullname = fullname;
this.birthDate = birthDate;
this.avaUrl = avaUrl;
this.isDisabled = isDisabled;
}

toString() {
    return `${this.id}) ${this.login} [ ${this.role} ]
    Full Name : ${this.fullname} BirthDay : ${this.birthDate} isActive [ ${this.isDisabled} ]
    Avatar Url : ${this.avaUrl}
    `;
}


static getDyId(id) {
    let rawdata = fs.readFileSync('models/data.json');  
    let student = JSON.parse(rawdata);
    const Users = student.items;
    return Users.find( x => x.id === id);
}

static getAll(){
    let rawdata = fs.readFileSync('models/data.json');  
    let student = JSON.parse(rawdata);
    const Users = student.items;
    return Users;
}

static getDyIdAsync(id, callback) {
    let rawdata = fs.readFile('models/data.json', (err, data) => {
        let student = JSON.parse(data);
        const Users = student.items;
        callback(err, Users.find( x => x.id === id));
      
    });
}

static getAllAsync(callback){
    return fs.readFile("models/data.json", (err, data) => {
        callback(err, JSON.parse(data).items);
    });
}

};

module.exports = User;
new User();
//}
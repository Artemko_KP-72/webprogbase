


app.get('/tests', function(req, res, next) {
    
        test_c.getAll((err, data) =>{
            if(err){
                res.status(404).send("Error 404");
            }
            else { 
                let curPage = 0;
                let Next =  "visibility: visible";
                let Prev =  "visibility: hidden";
            
                if(req.query.page){
                    curPage = parseInt(req.query.page);
                    if (curPage != 0){
                        Prev = "visibility: visible";
                    }
                }        
                let isSearch = false;
                let searchData = [];
                if(req.query.search){
                    isSearch = true;
                    let counter = 0;
                    for(let temp of data){
    
                        if (~temp.name.indexOf(req.query.search)) {
                            searchData[counter] = temp;
                            counter++;
                        }
                    }
                }
    
                let pageCount;
                if(isSearch){
                    pageCount = test_c.getSize(searchData);
                }else{
                    pageCount = test_c.getSize(data);     
                }
                let err = "";
                if(pageCount == 0 && isSearch){
                    err = "Not found";
                }else if(pageCount == 0){
                    err = "Empty list";
                }
                if(pageCount >=curPage*5){
                
                        let arr;
                        
                            if(pageCount <= (curPage+1)*5){
                                  
                                if(!isSearch){
                                    arr = data.slice(curPage * 5, pageCount);  
                                }
                                  
                                else{
                                    arr = searchData.slice(curPage * 5, pageCount);
                                }      
                                Next = "visibility: hidden";
                              
                            }else{
    
                                if(!isSearch){
                                    arr = data.slice(curPage * 5, (curPage+1)*5);
                                  }
                                else{
                                    arr = searchData.slice(curPage * 5, (curPage+1)*5);
                                  }     
                            }           
                        let findField = "";
                        let findParam = "";
                        if(isSearch){
    
                            findField = req.query.search;
                            findParam = "&search="+findField;
                        
                        }
                        let pageAmont = parseInt((pageCount/5.0)+0.8);
                        
                        if(pageAmont == 0) pageAmont = 1;
    
                        const testsData = {
                            "tests_all": arr,
                            "prev": Prev,
                            "next": Next,
                            "nextPage":curPage+1,
                            "prevPage":curPage-1,
                            "curPage":curPage+1,
                            "pageCount":pageAmont,
                            "findField":findField,
                            "findParam":findParam,
                            "err":err
                        };
    
                        res.setHeader('Content-Type', 'text/html');
                        res.render('tests', testsData);
                
                }
                else{
                    res.status(404).send("Error 404");  
                }
            }
        });
    
       
    
    });
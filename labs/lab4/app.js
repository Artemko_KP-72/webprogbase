const express = require('express');
const User = require("./models/user.js");
const Scores = require("./models/scores.js");
const fs = require('fs');
var multer = require('multer');
const path = require('path');
const mustache = require("mustache-express");
const busboyBodyParser = require('busboy-body-parser');
var bodyParser = require('body-parser');

const app = express(); 
const PORT = 5050;
app.listen(PORT); 
//view engine setup 

// will open public/ directory files for http requests
app.use(express.static("public"));
app.use(busboyBodyParser()); 
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.set('view engine', 'ejs');
app.use(busboyBodyParser({ limit: '5mb' }));


app.engine("mst", mustache(path.join(__dirname, "views", "partials"))); 
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "mst");
/*
*/

app.get("/home", function(req, res) { res.render('home'); });


app.get("/user/:id", function(req, res) {
    User.getAllAsync( (error, allUsers) => { 
        User.getDyIdAsync(parseInt(req.params.id), (err, user) => {


            if (typeof(user) === "undefined")
                res.end(`Sorry, user with id = \"${req.params.id}\" is deleted or does not exist`);
            else {
                const St_arr = [];

                for (let singleUser of allUsers)
                    if (typeof(singleUser) !== "undefined" && singleUser.id !== user.id && singleUser.role !== 1 )
                        St_arr.push(singleUser);

                const container = {
                    self : user,
                    studs : St_arr, // why this.self is undefined yet? i gues its fill after command procces
                    prediction : getRandPrediction()
                };

                res.render('user', container);
            }
        });
    });
});

app.get("/users", function(req, res) {
        User.getAllAsync((err, allUser) => {
            
            const St_arr = [];
            const T_arr = []; 

            for (let user of allUser)
                if (typeof(user) !== "undefined")
                    if  (user.role === 1)
                        T_arr.push(user);
                    else St_arr.push(user);

            const container = {
                studs : St_arr, 
                teachers : T_arr 
            };   
            res.render('users', container);
        });
 });

 app.get("/score/:id", function(req, res) { 
 
    Scores.getDyIdAsync(parseInt(req.params.id), (error, self) => { 

        if (error) {
            console.log(error.toString());
            res.sendStatus(404);
        } 

        if (typeof(self) === "undefined")
            res.end(`Sorry, user with id = \"${req.params.id}\" is deleted or does not exist`);
        else {
            User.getDyIdAsync(parseInt(self.teacherId), (St_err, TEACHER) => {
                User.getDyIdAsync(parseInt(self.studentId), (T_err, St) => {

                    if (St_err) res.send(St_err.toString());
                    if (T_err) res.send(TEACHER_err.toString());
  
                    const del = "This user was deleted from data base";
                    const container = { 
                        Reciver : typeof(St) === "undefined" ? del : St.fullname ,
                        ReciverUrl : "http://localhost:5050/user/" + St.id,
                        id : self.id,
                        linkToDel : "http://localhost:5050/delete/" + self.id,
                        Date : self.date,
                        Mark : self.mark, 
                        Type : self.type,
                        Subject : typeof(TEACHER) === "undefined" ? del : TEACHER.login,
                        Comment : self.comment,
                        teacherUrl : typeof(TEACHER) === "undefined" ? del : TEACHER.selfLink,
                        teacher : typeof(TEACHER) === "undefined" ? del : TEACHER.fullname
                    };  

                    res.render('scoreInf', container);
                });
            });
        }
    });
});
 
 app.get("/scores", function(req, res, next) {

    
     // const temp = "lol"; // why this string is available in the other function without any common memory container
    Scores.getAllAsync((err, data) => { 
        if (err) res.end(err.toString()); 
        User.getAllAsync((error, users) => {
            

            const inputStr = require('url').parse(req.url, true).query.filter;
                        //console.log(temp); // here we use unavailable block of memory
           
            if (error) res.end(error.toString());

            let arr = []; 

            for (let score of data)
                if (score !== null && typeof(score) !== "undefined" && isContainKeyWord(score, inputStr, users[parseInt(score.studentId)], users[parseInt(score.teacherId)]))
                // console.log(users[parseInt(score.studentId)]);
                    arr.push({
                            id : score.id,
                            fullname :users[parseInt(score.studentId)].fullname,
                            mark : score.mark,
                            subj : users[parseInt(score.teacherId)].login
                        });
        
            const standart = 3;
            const scoresPerPage = (typeof(req.params.scoresPerPage) === "undefined") ? standart : parseInt(req.params.scoresPerPage); 
            const page = (typeof(req.query.page) === "undefined") ? 1 : parseInt(req.query.page) ;
            const maxPageCount = getPageSize(arr, scoresPerPage);

            const container = {  
                scores : getScoresList(arr, page, scoresPerPage),
                prediction : getRandPrediction(), 
                data : inputStr,
                prevPage : page > 1 ? (page - 1) : 0,
                currPage : page, 
                overall : maxPageCount,    
                nextPage : page < maxPageCount ? (page + 1) : 0 
            };  
            console.log(arr.length === 0);
            if (arr.length === 0)
                res.render("scores", { data : inputStr,    notFound: "Nothing have been found"}); 
            res.render('scores', container);
        });
    });
 }); 
 
app.get("/pushMark", (req, res) => {
    res.render("./Entities/new");
    console.log("vse srabotalo");
});

app.get("/add_new_mark", function(req, res) {
    User.getAllAsync((err, users) => {
        console.log(require('url').parse(req.url,true).query);
        const input = require('url').parse(req.url,true).query;
        // if (input.stud_name) // cheak name or id // implement name first
        let studId = -1;
        let teachId = -1;
        for (let singleUser of users)
            if (typeof(singleUser) !== "undefined")
                if (singleUser.fullname === input.stud_name)
                    studId = singleUser.id;
                else if (singleUser.fullname === input.teach_name) teachId = singleUser.id;

        if (studId === -1)  res.end("Sorry, can`t find user " + `${input.stud_name}` + " please, check the correctness of the entered data" );
        if (teachId === -1)  res.end("Sorry, can`t find user " + `${input.teach_name}` + " please, check the correctness of the entered data" );
        // there is no need to cheak type becouse in frontend part it`s radio button
        // if (input.type === "Exam" && input.type !== "HomeTask" && input.type !== "Test" && input.type !== "ClassWork")
        if (typeof(input.mark) == "undefined" || isNaN(input.mark) === true || input.mark < 0 || input.mark > 12) 
            res.end("There is no such mark as " + `${input.mark}`);
        const newScore = {
                id : 0,
                studentId : studId.toString(),
                teacherId : teachId.toString(),
                mark : input.mark.toString(),
                date : (new Date()).toString(),
                comment : input.comment,
                type : input.type
                }; 
 
        Scores.insert(newScore);
        Scores.getAllAsync((err, data) => { 
            res.redirect("/score/" + (data.length - 1).toString());
        });
    });
});
 
app.post("/delete/:id", (req, res) => {
    console.log(req.params.id);
    Scores.deleteScore(parseInt(req.params.id));
    res.redirect("/scores");
}); 

app.post("/user_register/", (req, res) => { 

    const avaFile = req.files.avatar;
    const fileName = req.body.fname + avaFile.name.substring(avaFile.name.lastIndexOf("."), avaFile.name.length);
    const path = "/home/artemko/webprogbase/labs/lab4/public/images/" + fileName;
    fs.writeFileSync(path, avaFile.data);
 
    let data = { 
        fileLink: "/images/" + fileName
    };
    console.log("/image/" + fileName);
    res.render('fileview', data);

    });

app.get("/home", function(req, res) { res.render('home'); });
app.get("/about", function(req, res) { res.render('about'); });
app.get("/sign_in_or_up", function(req, res) { 
    //res.send('Sorry, this option is unavailable now, but i am working on it)');
    res.render("user_registration_form");
});
app.get("/:path", function(req, res) { res.send(`Sorry path /"${req.params.path}/" is unavailable or does not exist`); });

function isContainKeyWord(score, inputStr, student, teacher) {
    if (inputStr === undefined 
        || score.date.includes(inputStr)
        || score.comment.includes(inputStr)
        || score.type.includes(inputStr)
        || score.mark.includes(inputStr) 
        || student.fullname.includes(inputStr) 
        || student.birthDate.includes(inputStr) 
        || teacher.birthDate.includes(inputStr)
        || teacher.login.includes(inputStr)
        || teacher.fullname.includes(inputStr)) return true;
    return false;
}

function getPageSize(arr, scoresPerPage) {
    return Math.ceil( arr.length / scoresPerPage );
} 

function getScoresList(arr, page, scoresPerPage) {  
    const res = [];  
    --page;  
 
    for (let i = page * scoresPerPage; i < page * scoresPerPage + scoresPerPage && i < arr.length; i++)
        res.push(arr[i]); 
    return res;
}

function getRandPrediction() {
    const database = readWebDoc("./data/predictions.txt").split("\n");
    // Return a random number between 1 and 100:
    return database[Math.floor((Math.random() * database.length))];
}

function readWebDoc(fileName) {
    console.log(__dirname);
    const filePath = path.join(__dirname, fileName);
    return fs.readFileSync(filePath).toString();
};
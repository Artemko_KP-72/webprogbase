const fs = require('fs');
const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    // id:     {type: Number, required: true},
    login:  {type: String, required: true},
    role:   {type: Number, default: 0},
    fullname:{type:String, required: true},
    birthDate: {type: Date, required: true},
    avaUrl: {type: String, default: "some Default link"},
    selfLink: {type: String, default: "/user/0"},
    isDisabled: {type: Boolean, default: true},
});

const UserModel = mongoose.model('User', UserSchema);

class User {

// let myCallBackFunction = function(){};

// constructor used for tests
// constructor (){}

constructor (id, login, role, fullname, birthDate, avaUrl, selfLink, isDisabled) {
    this.id = id;
    this.login = login;
    this.role = role;
    this.fullname = fullname;
    this.birthDate = birthDate;
    this.avaUrl = avaUrl;
    this.selfLink = selfLink;
    this.isDisabled = isDisabled;
}

toString() {
    return `${this.id}) ${this.login} [ ${this.role} ]
    Full Name : ${this.fullname} BirthDay : ${this.birthDate} isActive [ ${this.isDisabled} ]
    Avatar Url : ${this.avaUrl}
    `;
}


static getDyId(id) {
    return UserModel.find({ "_id" : id });
}

static getAll(){
    return UserModel.find();
}

static delete() {
    return UserModel.remove();
}

static getDyIdAsync(id, callback) {
    let rawdata = fs.readFile('models/data.json', (err, data) => {
        let student = JSON.parse(data);
        const Users = student.items;
        callback(err, Users.find( x => x.id === id));
      
    });
}

static getAllAsync(callback){
    return fs.readFile("models/data.json", (err, data) => {
        callback(err, JSON.parse(data).items);
    });
}

static getDyIdSync(id) {
    UserModel.findById(id)
        .then( ent => {return ent});
}

static insert(user){
    return new UserModel(user).save();
}

};

module.exports = User;
new User();
//}
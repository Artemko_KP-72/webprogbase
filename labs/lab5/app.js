const mongoose = require('mongoose');

const User = require('./models/user.js');
const fs = require('fs');
const path = require('path');
const express = require('express');
const mustache = require('mustache-express');
const app = express();
const bodyParser = require('body-parser');
const multer = require('multer');
const busboyBodyParser = require('busboy-body-parser');
const userRouter = require("./routes/UsersRequestHandler");
const userRouterScores = require("./routes/ScoresHandler");
const userRouterTables = require("./routes/TablesHandler");
const Scores = require('./models/scores');

app.use("/", userRouter);
app.use("/", userRouterScores);
app.use("/", userRouterTables);
app.use(bodyParser.urlencoded({extended: true}));
//view engine setup 

// will open public/ directory files for http requests
app.use(express.static("public"));
app.use(busboyBodyParser()); 
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.set('view engine', 'ejs');
app.use(busboyBodyParser({ limit: '5mb' }));


app.engine("mst", mustache(path.join(__dirname, "views", "partials"))); 
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "mst");

app.engine("mst", mustache(path.join(__dirname, "views", "partials"))); 
app.set("views", path.join(__dirname, "views"))
app.set("view engine", "mst");
 
app.get("/", (req, res) => {
    User.getAll()
        .then(users =>res.render("index")); 
}); 



const databaseUrl = 'mongodb://localhost:27017/lab5db';
const serverPort = 6035;

const connectOptions = {useNewUrlParser: true};

mongoose.connect(databaseUrl, connectOptions)
    .then(() => console.log(`Database connected: ${databaseUrl}`))
    .then(() => app.listen(serverPort, () => console.log(`Server started: ${serverPort}`)))
    .catch(err => console.log(`Some problem detected: ${err}`));


app.post("/", (req, res) => {
    if (false)
    res.send("ok");  
    else
        // User.getAllAsync((err, data) => { 
        //     res.render('index', {users: data})
        // });
    User.getAll()  
        .then((data) => res.render('index', {users: data}))
}); 



app.get("/home", function(req, res) { res.render('home'); });

app.get("/delete", (req, res) => {
    User.delete()
        .then( () => { res.send("All Users was deleted!")});
});


app.get("/updateForm/:id", (req, res) => { 
    Scores.getDyId(req.params.id)
        .then(data => {
            const ent = {linktoUpdate: "/change_new_mark/"+ data._id + "/",
                        data: data}
            res.render('./Entities/update', ent);     
        })
});
 
app.get("/pushMark", (req, res) => {
    res.render("./Entities/new");
    console.log("vse srabotalo");
});

app.get("/fill", (req, res) => {
    User.getAllAsync((err, AllUsers) => {
        for (let data of AllUsers) {
            User.insert(new User(data.id, data.login, data.role, data.fullname, new Date(data.birthDate), data.avaUrl, data.selfLink, data.isDisabled))
                .then(entity => {console.log(entity);})
                .catch(err => {console.log(err.toString());});
        }
        res.send("all Users is added");
    });
});

app.post("/user_register/", (req, res) => { 

    const avaFile = req.files.avatar;
    const fileName = req.body.fname + avaFile.name.substring(avaFile.name.lastIndexOf("."), avaFile.name.length);
    const path = "/home/artemko/webprogbase/labs/lab5/public/images/" + fileName;
    fs.writeFileSync(path, avaFile.data);
 
    let data = { 
        fileLink: "/images/" + fileName
    };
    console.log("/image/" + fileName);
    res.render('fileview', data);

    });

app.get("/home", function(req, res) { res.render('home'); });
app.get("/about", function(req, res) { res.render('about'); });
app.get("/sign_in_or_up", function(req, res) { 
    //res.send('Sorry, this option is unavailable now, but i am working on it)');
    res.render("user_registration_form");
});
app.get("/:path", function(req, res) { res.send(`Sorry path /"${req.params.path}/" is unavailable or does not exist`); });

function getRandPrediction() {
    const database = readWebDoc("./data/predictions.txt").split("\n");
    return database[Math.floor((Math.random() * database.length))];
}

function readWebDoc(fileName) {
    console.log(__dirname);
    const filePath = path.join(__dirname, fileName);
    return fs.readFileSync(filePath).toString();
};
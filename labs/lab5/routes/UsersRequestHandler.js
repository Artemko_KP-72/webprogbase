const express = require('express');
const router = express.Router();
const User = require('../models/user');
const bodyParser = require('body-parser');

router.use(bodyParser.urlencoded({extended: true}));

router.get("/users", function(req, res) {
    User.getAll()
        .then(users => res.render("users", {users: users}));
 });

router.post("/users", (req, res) => {
    const userLogin = req.body.login; 
    if (userLogin === undefined)
        User.getAll()
            .then(users => res.render("users", {users: users}));
        console.log(typeof(userLogin));
    User.insert(new User(0, userLogin, 0, "Ivan Ivanovich Ivanov", new Date(),"avaUrl", "/user/", true))  // other will initialize as undefined
        .then(() => 
            User.getAll()
                .then(users => res.render("users", {users: users})))
        .catch(err => res.status(500).send(err.toString()))
});
 

router.get("/user/:userID", (req, res) => {
    const ID = req.params.userID; // string
    console.log(ID);
    User.getDyId(ID.substring(1, ID.length))
        .then(user => { 
            console.log(user);
            if (user)
                res.render("user", user[0]);
            else res.status(404).send(`User with id ${ID} not found`);
        })
        .catch(err => res.send(err.toString()))
    });

    function isContainKeyWord(score, inputStr, student, teacher) {
        if (inputStr === undefined 
            || score.date.toString().includes(inputStr)
            || score.comment.includes(inputStr)
            || score.type.includes(inputStr)
            || score.mark.toString().includes(inputStr) 
            || student.fullname.includes(inputStr) 
            || student.birthDate.toString().includes(inputStr)
            || teacher.login.includes(inputStr)
            || teacher.fullname.includes(inputStr)) return true;
        return false;
    }

module.exports = router;
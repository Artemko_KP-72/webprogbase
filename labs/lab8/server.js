// set up ======================================================================
// get all the tools we need
const express  = require('express');
const app      = express();
const port     = process.env.PORT || 8080;
const mongoose = require('mongoose');
const passport = require('passport');
const flash    = require('connect-flash');  
const path = require('path');
const bodyParser = require('body-parser');
const mustache = require('mustache-express');
const multer = require('multer');
const busboyBodyParser = require('busboy-body-parser'); 
const morgan       = require('morgan');
const cookieParser = require('cookie-parser');
const session      = require('express-session');

const configDB = require('./config/database.js');

// configuration ===============================================================
mongoose.connect(configDB.url); // connect to our database

require('./config/passport')(passport); // pass passport for configuration

// set up our express application
// app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms
app.use(express.static("public"));




app.use(busboyBodyParser()); 
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
app.use(busboyBodyParser({ limit: '5mb' }));

app.engine("mst", mustache(path.join(__dirname, "views", "partials"))); 
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "mst"); // set mst as template engine

// required for passport
app.use(session({ secret: 'ilovescotchscotchyscotchscotch' })); // session secret
app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// routes ======================================================================
require('./app/routes/auth.js')(app, passport); // load our routes and pass in our app and fully configured passport
require('./app/routes/user.js')(app, passport);
require('./app/routes/table.js')(app, passport);
require('./app/routes/scores.js')(app, passport);

// API
const apiRouter = require('./app/routes/api.js')(app, passport);
require('./app/routes/developer.js')(app, passport);

// launch ======================================================================
app.listen(port);
console.log(`Server started http://localhost:${port}`);  


app.get("/:path", function(req, res) { res.send(`Sorry path /"${req.params.path}/" is unavailable or does not exist`); });
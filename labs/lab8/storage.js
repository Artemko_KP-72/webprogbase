
// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

// create the model for users and expose it to our app
module.exports = mongoose.model('User', userSchema);








// load the things we need
var mongoose = require('mongoose');
var bcrypt   = require('bcrypt-nodejs');

// define the schema for our user model
var userSchema = mongoose.Schema({

    local            : {
        username     : String,
        password     : String,
        role         : String,
        fullname     : String, 
        birthDate    : String,
        avaUrl       : String,
        selfLink     : String,
        isDisabled   : String
    }

});


const UserModel = mongoose.model('User', userSchema);

// methods ======================
// generating a hash
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

// checking if password is valid
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};




userSchema.methods.toString = () => {
    return `${this.id}) ${this.login} [ ${this.role} ]
    Full Name : ${this.fullname} BirthDay : ${this.birthDate} isActive [ ${this.isDisabled} ]
    Avatar Url : ${this.avaUrl}
    `;
}


userSchema.methods.getDyId = (id) => {
    return UserModel.find({ "_id" : id });
}

userSchema.methods.insert = (user) => {
    console.log("Vse yak vsigda");
    return new UserModel(user).save();
}

userSchema.methods.getAll = () => {
    return UserModel.find();
}

userSchema.methods.delete = () => {
    return UserModel.remove();
}

userSchema.methods.getDyIdAsync = (id, callback) =>  {
    let rawdata = fs.readFile('models/data.json', (err, data) => {
        let student = JSON.parse(data);
        const Users = student.items;
        callback(err, Users.find( x => x.id === id));
      
    });
}

userSchema.methods.getAllAsync = (callback) =>{
    return fs.readFile("models/data.json", (err, data) => {
        callback(err, JSON.parse(data).items);
    });
}

userSchema.methods.getDyIdSync = (id) => {
    UserModel.findById(id)
        .then( ent => {return ent});
}


// create the model for users and expose it to our app
module.exports = UserModel;


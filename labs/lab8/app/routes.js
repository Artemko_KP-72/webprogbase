const User            = require('../app/models/user');
const bodyParser = require('body-parser');
const multer = require('multer');
const busboyBodyParser = require('busboy-body-parser'); 

const fs = require('fs');
// app/routes.js
module.exports = function(app, passport) {


        app.use(busboyBodyParser());  
        app.use(bodyParser.json()); // support json encoded bodies
        app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies
        app.set('view engine', 'ejs');
        app.use(busboyBodyParser({ limit: '5mb' }));

    
        // =====================================
        // HOME PAGE (with login links) ========
        // =====================================
        app.get('/', function(req, res) {
            res.render('index.ejs'); // load the index.ejs file
        });
    
        // =====================================
        // LOGIN ===============================
        // =====================================
        // show the login form
        app.get('/login', function(req, res) {
    
            // render the page and pass in any flash data if it exists
            res.render('login.ejs', { message: req.flash('loginMessage') }); 
        });
    
        // process the login form
        // app.post('/login', do all our passport stuff here);
    
        // =====================================
        // SIGNUP ==============================
        // =====================================
        // show the signup form
        app.get('/signup', function(req, res) {
    
            // render the page and pass in any flash data if it exists
            res.render('signup.ejs', { message: req.flash('signupMessage') });
        });
    
        // process the signup form
        // app.post('/signup', do all our passport stuff here);
    
        // =====================================
        // PROFILE SECTION =====================
        // =====================================
        // we will want this protected so you have to be logged in to visit
        // we will use route middleware to verify this (the isLoggedIn function)
        app.get('/profile', (req, res) => {
            if (isLoggedIn(req)) {
                res.render('profile.ejs', {
                    user : req.user // get the user out of session and pass to template
                });
                console.log(req.user);
            }
            else res.send("sorry, you not logged in");
        });
    
        // =====================================
        // LOGOUT ==============================
        // =====================================
        app.get('/logout', function(req, res) {
            req.logout();
            res.redirect('/');
        });

         // process the signup form
    app.post('/signup', (req, res) =>  {

        User.findOne({ 'username' :  req.body.username }, (err, user) => {
            // if there are any errors, return the error before anything else
            if (err) {
                console.log(err.toString());
                return;
            }

            // if no user is found, return the message
            if (user) {
                console.log(user);
                res.send("This username Allready taken!");
            } else {

                    console.log(req.body);
                    
                    let fileName = "defaultImage.svg";

                    if (req.files.avatar !== undefined) {
                        const avaFile = req.files.avatar;
                        fileName = req.body.username + avaFile.name.substring(avaFile.name.lastIndexOf("."), avaFile.name.length);
                        const path = "/home/artemko/webprogbase/labs/sesion_training/public/images/" + fileName;
                        fs.writeFileSync(path, avaFile.data);
                    }

                    User.schema.methods.insert( 
                        {
                            username     : req.body.username,
                            password     : User.schema.methods.generateHash(req.body.password).toString(),
                            role         : req.body.role || 0,
                            fullname     : req.body.fullname,
                            email        : req.body.email,
                            birthDate    : req.body.birthDate,
                            avaUrl       : "images/" + fileName, 
                            selfLink     : "users/",
                            isDisabled   : false,//req.body.isDisabled,
                        })
                        .then(answ => {
                            User.schema.methods.getById(answ._id)
                                .then( newUser => {
                                    res.send(newUser);
                                }) 
                            .catch(err => res.send(err.toString())); 
                        })  //res.send(answ))
                        .catch(err => res.send(err.toString()));
                    }
                });
});

        // process the signup form
    app.get('/getAll', (req, res) =>  {
        User.schema.methods.getAll()
            .then( (data) => {
                console.log(data);
                res.send("ok");
            })
            .catch(err => res.send(err.toString));
    });

    app.post('/login', passport.authenticate('local-login', {
        successRedirect : '/profile', // redirect to the secure profile section
        failureRedirect : '/login', // redirect back to the signup page if there is an error
        failureFlash : true // allow flash messages
    }));

    };
    
    // route middleware to make sure a user is logged in
    function isLoggedIn(req) {
        return req.isAuthenticated();
    }
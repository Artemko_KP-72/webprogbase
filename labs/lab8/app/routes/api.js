const express  = require('express'); 
const api = express.Router();
const auth = require('basic-auth');
const fs = require('fs');
const passport = require("passport");
const BasicStrategy = require('passport-http').BasicStrategy;
const app       = express();     
const router = express.Router(); 
var telegram = require('telegram-bot-api');

// models
const User   = require('../models/user');
const Scores = require('../models/scores');
const Table = require('../models/table');

// default values
const standartEmail = "someGroupEmail@gmail.com";
const standartAvaUrl = "standartAva.jpg"

const igrushki = require('../../test');

const telegramLog = new telegram({
    token: '718270456:AAHjWuD97gklY8tXU8x1Qsig4_vp0LPijMo',
    updates: {enabled: true}
  });
  

module.exports = function(api, passport) {

    api.get('/api/v1/private', 
    passport.authenticate('basic', { session: false }),
    (req, res) => {
        if (req === null) res.json("Acces denied");
        else res.json(JSON.stringify(req.user, 4, null));
    });
    
    router.use(function(req, res, next) {

        const text = req.user + "\n" + req.originalUrl + "\n" + req.user._id;
        
        console.log(text);

        telegramLog.sendMessage(
                   {chat_id: "428250742", text: "Hello"})
                .then(() => console.log("message sent"))
                .catch(err => console.log("Error:", err.toString()));
          
        // https.get('https://api.telegram.org/bot718270456:AAHjWuD97gklY8tXU8x1Qsig4_vp0LPijMo/sendMessage?chat_id=428250742&text=' + text, (resp) => {   console.log(resp)   });
        next(); // make sure we go to the next routes and don't stop here
    });

    passport.use(new BasicStrategy((userid, password, done)=>{
        User.find({"login": userid.toString()})
            .then( user => {
                if (user[0] != undefined && User.schema.methods.validPassword(user[0], password.toString()))
                    done(null, user[0]);
                else  done(null,false);
            })
            .catch(err => done(false));
    }));

    
// USERS CRUD
// =============================================================================

    api.get('/api/v1/me',checkUser, function(req, res){
            res.json(req.user);
     });

    api.post("/api/v1/users", passport.authenticate('basic', { session: false }), 
    (req, res) => {

        const input = require('url').parse(req.url,true).query;
        const count = input.unitsPerPage | 5;
        let page = input.page | 1;
        page--;

        filtUsers(input.filter || "")
            .then(users => {
                const newArray = [];
                    for (let i = count * page; i < (count * (page + 1)) && users[i] !== undefined; i++)
                        newArray.push(users[i]);

                    if (newArray.length === 0) res.send("Sorry this page is unavailable");
                    else res.json(newArray);
                    })
                    .catch(err => res.send("Can`t find Users/n Place api.post(\"/api/v1/users\"\n" + err.toString()));
    });

    api.put("/api/v1/updateSelfInfo", passport.authenticate('basic', { session: false }), 
    (req, res) => {
        const input = require('url').parse(req.url, true).query;
        User.findById(input._id | req.user._id)
            .then(user => {

                if (user === null) res.json({"message": "User not found"});
                else    if ((req.user.role === '1' && user.role === '0') || req.user.login === user.login) {
                    User.schema.methods.update(user, input)
                    .then(answ => res.json(answ))
                    .catch(err => res.json(err.toString()));
                }
                else res.json("failure");
            })
            .catch(err => res.json({"Error: ": err.toString(), "place" : "updateSelfInfo"}));    
    });

    api.post("/api/v1/rookie", // passport.authenticate('basic', { session: false }), 
    (req, res) => {    
        const input = require('url').parse(req.url, true).query;
        let isValidUser = true;
        if (input.login && input.password && input.fullname && new Date(input.birthDate)) {
            const rookie =
                {
                    login: input.login,
                    password: User.schema.methods.generateHash(input.password).toString(),
                    role: input.role || 0,
                    fullname: input.fullname,
                    email: input.email || standartEmail,
                    birthDate: (new Date(input.birthDate)).toString().substr(0, 25),
                    avaUrl: input.avaUrl || standartAvaUrl, // @TODO add "/images/" to pass
                    selfLink: "/user/",
                    isDisabled: false
                }

            User.schema.methods.insert(rookie)
                .then(answ => {
                    User.schema.methods.getById(answ._id)
                        .then( newUser => res.json(newUser)) 
                    .catch(err => res.send(err.toString())); 
                })  //res.send(answ))
                .catch(err => res.send(err.toString()));
        } else  res.json("Invalid input");    
    });

    api.delete("/api/v1/deleteme", passport.authenticate('basic', { session: false }), 
    (req, res) => {
        User.remove({_id: req.user._id})
            .then(data => res.json(data.toString()))
            .catch(err => res.json(err.toString()));
    });
    
            
// SCORES CRUD
// =============================================================================

api.get("/api/v1/scores/:id", passport.authenticate('basic', { session: false }), 
(req, res) => {
    console.log(req.params.id);
    if (!req.params.id) res.json("Incorect ID");
    else    Scores.getById(req.params.id.toString())
                .then(scores => res.json(scores))
                .catch(err => res.send(err.toString()));
});

api.put("v1/scores/:id"), passport.authenticate('basic', { session: false }), 
(req, res) => {

    console.log("LO");

    const input = require('url').parse(req.url, true).query;
    
    if (req.user.role === '0') {    res.send("You have no rigths to change this score");    return;}

    Scores.getById(req.params.id)
        .then(score => {
            Scores.update({id: req.params.id,
                studentId : score.studentId,
                teacherId : score.teacherId,
                mark: input.mark || score.mark,
                date: getNiceValidDate() + ` (Last updated by ${req.user.login})`,
                comment: input.comment || score.comment,
                type: input.type || score.type
            })
            .then(data => {
                res.redirect('/scores');
            }).catch(err => res.send(err.toString() + "\nPlace: api.put(\"/api/v1/scores/:id\") INNER"))
    }).catch(err => res.send(err.toString() + "\nPlace: api.put(\"/api/v1/scores/:id\")"))
}



api.delete("/api/v1/deleteScore", passport.authenticate('basic', { session: false }), 
(req, res) => {
    console.log(req.body);
    const input = req.body;//require('url').parse(req.url, true).query;
    console.log(input);
    if (input.id === undefined || input.id === null) res.send("Incorect parameters");
    else    Scores.delete(input.id)
                .then(data => res.json(data))
                .catch(err => res.send(err.toString()));
});

api.post("/api/v1/scores", passport.authenticate('basic', { session: false }), 
(req, res) => {
    
    const input = require('url').parse(req.url,true).query;
    const count = input.unitsPerPage | 5;
    let page = input.page | 1;
    page--;

    Scores.getAll()
        .then(scores => {
            
            const newArray = [];
            for (let i = count * page; i < (count * (page + 1)) && scores[i] !== undefined; i++) {
                newArray.push(scores[i]);
                console.log(scores[i]);
            }
            if (newArray.length === 0) res.send("Sorry this page is unavailable");
            else res.json(newArray);
        })
        .catch(err => res.send(err.toString()));
});

api.post("/api/v1/addScore", passport.authenticate('basic', { session: false }), 
(req, res) => {

    if (req.user.role !== '1') {    res.send("You have no rigths to add a new mark"); return;   }

    const input = req.body;//require('url').parse(req.url, true).query;

    console.log(input.stud_name);
    
          if (typeof(input.mark) == "undefined" || isNaN(input.mark) === true || input.mark < 0 || input.mark > 12)
            res.end("There is no such mark as " + `${input.mark}`);
          else  getUsersByNames(input.stud_name, input.teach_name)
                  .then( struct => {

                    console.log(struct);
                    if (!struct) {      res.send("Incorect data, remember you should enter fullname, not login!!");     return;}
                    if (struct.student === undefined || struct.student.role !== '0') {     res.send("Invalid name of student");     return;}
                    if (struct.teacher === undefined || struct.teacher.role !== '1') {     res.send("Invalid name of teacher");     return;}
    
                      Scores.insert({
                            id: 0,
                            studentId: struct.student._id.toString(),
                            teacherId: struct.teacher._id.toString(),
                            mark: input.mark.toString(),
                            date: getNiceValidDate(),
                            comment: input.comment | "-",
                            type: input.type
                          })
                          .then(
                              el => {
                                  Table.getAll()
                                      .then(data => {

    
                                        let SingleTableToUpdate = {};
                                        for (let singleTable of data) 
                                          if (el.teacherId.toString() ===
                                              singleTable.teacherId.toString())
                                            SingleTableToUpdate = singleTable;
    
                                        if (SingleTableToUpdate._id === undefined)
                                          res.send(
                                              "This teacher has no table, please create table first");
                                        else
                                          res.redirect(
                                              "/tableAddScore/" +
                                              SingleTableToUpdate._id.toString() +
                                              "?score_id=" + el.id);
    
                                      })
                                      .catch(err => {
                                        res.send(err.toString() + "\ncode: 1325");
                                        return;
                                      })})
                          .catch(err => {
                            res.send(err.toString() + "\ncode: 1342");
                            return;
                          });
                    }).catch(err => res.send(err.toString()) + "\ncode: 1398");
});


// TABLES CRUD
// =============================================================================

api.post("/api/v1/tables", (req, res) => {
    const input = require('url').parse(req.url,true).query;
    const count = input.unitsPerPage | 2;
    let page = input.page | 1;
    page--;
    Table.getAll()
        .then(tables => {
            const newArray = [];
            for (let i = count * page; i < (count * (page + 1)) && tables[i] !== undefined; i++) {
                newArray.push(tables[i]);
                console.log(tables[i]);
            }
            if (newArray.length === 0) res.send("Sorry this page is unavailable");
            else res.json(newArray);
        })
        .catch(err => res.send(err.toString() + "\ncode 1067"));
});

api.get("/api/v1/tables/:id", (req, res) => {
    Table.get(req.params.id)
        .then(tables => res.json(tables))
        .catch(err => res.send(err.toString() + "\ncode 1067"));
});


api.put("/api/v1/tables/:id", (req, res) => {
    const input = require('url').parse(req.url,true).query;
    Table.rename(req.params.id, input.newName)
        .then(info => {
            Table.get(req.params.id)
                .then(table => res.json(table))
                .catch(err => res.send(err.toString() + "\ncode 1082"));
        })
        .catch(err => res.send(err.toString() + "\ncode 1067"));
});


api.post("/api/v1/addTable", passport.authenticate('basic', { session: false }),
(req, res) => {
    const input = require('url').parse(req.url,true).query;

    if (req.user.role !== '1') {    res.send("You have no rigths to add a table"); return;   }

    User.findOne({ 'fullname' :  req.user.fullname }, (err, user) => {

        if (err) {      res.send(err.toString());  return;    }

        Table.init(req.user._id, input.journalName)
            .then(el => res.json(el))
            .catch(err => res.send(err.toString()));

    });
});

api.delete("/api/v1/deleteTable", passport.authenticate('basic', { session: false }),
(req, res) => {

    if (req.user.role !== '1') {    res.send("You have no rigths to add a table"); return;   }
    console.log(typeof req.user._id);
    Table.getAll()
        .then( tables => {
            let wanted;
            for (let singleTable of tables)
                if (singleTable.teacherId === req.user._id.toString())
                    wanted = singleTable;
            console.log(wanted);
            
        Table.delete(wanted._id.toString())
            .then(info => res.json(info))
            .catch(err => res.send(err.toString() + "\ncode: 4567"));
        })
        .catch(err => res.send(err.toString() + "\ncode: 4542"));
});

}

function filtUsers(filter) {
    return User.schema.methods.getAll()
        .then(rawUsers =>
            {
                const users = [];
                for (let i = 0; i < rawUsers.length; i++) {
                    if (rawUsers[i].login.includes(filter) || rawUsers[i].fullname.includes(filter)
                     || rawUsers[i].role.includes(filter) || rawUsers[i].birthDate.includes(filter)
                     || rawUsers[i].email.includes(filter))
                        users.push(rawUsers[i]);
                }
                
                return users;
            })
            .catch(err => Promise.reject("can`t find users/nPlace: filtUSers"));
}


function checkUser (req,res,next){
    passport.authenticate('basic', { session: false}, 
    function(err, user, info) {
        if (err) { return res.json(err.message); }
        if (!user) { return res.json("Invalid username or password.")}
        req.logIn(user, function(err) {
          if (err) { return res.json({"error":err.message}); }
          next();
        });
    })(req, res);
}


function getUsersByNames(st_Name, teacher_Name) {
    return User.schema.methods.getAll()
        .then(users => {
          let st = -1;
          let teacher = -1;
          for (let singleUser of users) {
            if (typeof(singleUser) !== "undefined")
              if (singleUser.fullname !==
                  undefined)  // should be deleted after database will renew
                if (singleUser.fullname.includes(st_Name))
                  st = singleUser;
                else if (singleUser.fullname.includes(teacher_Name))
                  teacher = singleUser;
          }
  
          if (st === -1)
            Promise.reject(
                "Sorry, can`t find student " + `${st_Name}` +
                " please, check the correctness of the entered data");
          else
  
          if (teacher === -1)
            Promise.reject(
                "Sorry, can`t find teacher " + `${teacher_Name}` +
                " please, check the correctness of the entered data");
          else
              return {student: st, teacher: teacher};
        })
        .catch(err => Promise.reject(err));  //`nsource: getUsersByNames\nInput
                                     //parameters: st_Name = ${st_Name}\n
                                     //teacher_Name = ${teacher_Name}`));
  }

  function getNiceValidDate() {
    return (new Date()).toString().substr(0, 25);
  }

function isLoggedIn(req) {
    return req.isAuthenticated();
}
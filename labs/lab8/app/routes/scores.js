const Scores = require('../models/scores');
const User = require('../models/user');
const Predictator = require('../predictions/predictions');
const Table = require('../models/table');

const path = require('path');
const fs = require('fs');
// app/routes.js
module.exports =
    function(app, passport) {

  app.get("/scores", (req, res, next) => {

    if (!isLoggedIn(req)) {
      res.redirect("/home");
      return;
    }

    Scores.getAll()
        .then(data => {
          User.schema.methods.getAll()
              .then(users => {


                const inputStr =
                    require('url').parse(req.url, true).query.filter;

                let arr = [];

                for (let score of data)
                  if (score !== null && typeof(score) !== "undefined" &&
                      isContainKeyWord(score, inputStr, getUserById(users, score.studentId), getUserById(users, score.teacherId)))
                      if (score.studentId && score.teacherId)
                        arr.push({
                          id: score.id,
                          fullname:
                              getUserById(users, score.studentId)
                                  .fullname,  //  users[parseInt()].fullname,
                          mark: score.mark,
                          subj:
                              getUserById(users, score.teacherId)
                                  .fullname  // users[parseInt(score.teacherId)].login
                        });

                const standart = 3;
                const scoresPerPage =
                    (typeof(req.params.scoresPerPage) === "undefined") ?
                    standart :
                    parseInt(req.params.scoresPerPage);
                const page = (typeof(req.query.page) === "undefined") ?
                    1 :
                    parseInt(req.query.page);
                const maxPageCount = getPageSize(arr, scoresPerPage);

                if (arr.length === 0)
                  res.render("scores/scores", {
                    data: inputStr,
                    prediction: Predictator.getWishForUser(req.user._id),
                    user: req.user,
                    notFound: "Nothing have been found"
                  });
                else {
                  const container = {
                    user: req.user,
                    scores: getScoresList(arr, page, scoresPerPage),
                    prediction: Predictator.getWishForUser(req.user._id),
                    data: inputStr,
                    prevPage: page > 1 ? (page - 1) : 0,
                    currPage: page,
                    overall: maxPageCount,
                    nextPage: page < maxPageCount ? (page + 1) : 0
                  };
                  res.render('scores/scores', container);
                }
              })
              .catch(err => res.send(err.toString()));
        })
        .catch(err => res.send(err.toString()));
  });

  app.get("/add_new_mark", (req, res) => {

    if (!isLoggedIn(req) || parseInt(req.user.role) !== 1) {
      res.send("You have no rigths to add a new mark");
      return;
    }

    const input = require('url').parse(req.url, true).query;

      if (typeof(input.mark) == "undefined" || isNaN(input.mark) === true || input.mark < 0 || input.mark > 12)
        res.end("There is no such mark as " + `${input.mark}`);
      else  getUsersByNames(input.stud_name, "Test")
              .then( struct => {

                  Scores.insert({
                        id: 0,
                        studentId: struct.student._id.toString(),
                        teacherId: req.user._id.toString(),
                        mark: input.mark.toString(),
                        date: getNiceValidDate(),
                        comment: input.comment,
                        type: input.type
                      })
                      .then(
                          el => {
                              Table.getAll()
                                  .then(data => {

                                    let SingleTableToUpdate = {};
                                    for (let singleTable of data) 
                                      if (el.teacherId.toString() ===
                                          singleTable.teacherId.toString())
                                        SingleTableToUpdate = singleTable;

                                    if (SingleTableToUpdate._id === undefined)
                                      res.send(
                                          "This teacher has no table, please create table first");
                                    else
                                      res.redirect(
                                          "/tableAddScore/" +
                                          SingleTableToUpdate._id.toString() +
                                          "?score_id=" + el.id);

                                  })
                                  .catch(err => {
                                    res.send(err.toString());
                                    return;
                                  })})
                      .catch(err => {
                        res.send(err.toString());
                        return;
                      });
                }).catch(err => res.send(err.toString()));
  });

  app.get("/score/:id", function(req, res) {

    Scores.getById(req.params.id)
        .then(self => {

          if (typeof(self) === null)
            res.end(
                `Sorry, score with id = \"${req.params.id}\" is deleted or does not exist`);
          else {
            User.schema.methods.getById(self.teacherId).then(TEACHER => {
              User.schema.methods.getById(self.studentId).then(St => {

                const del = "This user was deleted from data base";
                const container = {
                  user: req.user,
                  Reciver: typeof(St) === "undefined" ? del : St[0].fullname,
                  ReciverUrl: "/user/" + St[0]._id,
                  id: self.id,
                  linkToDel: "/deleteScore/" + self.id,
                  linkToUpdate: "/updateForm/" + self.id,
                  Date: self.date,
                  Mark: self.mark,
                  Type: self.type,
                  Subject: typeof(TEACHER) === "undefined" ? del :
                                                             TEACHER[0].login,
                  Comment: self.comment,
                  teacherUrl: typeof(TEACHER) === "undefined" ? del : "/user/" +
                          TEACHER[0].id,
                  teacher: typeof(TEACHER) === "undefined" ?
                      del :
                      TEACHER[0].fullname,
                  _id: self._id
                };

                res.render('scores/scoreInf', container);
              });
            });
          }
        })
        .catch(err => res.send(err.toString()));
  });


  app.get("/update/:id", (req, res) => {

    const input = require('url').parse(req.url, true).query;

    getUsersByNames(input.student, input.teacher)
        .then(struct => {

                Scores.update({
                    id: req.params.id,
                    studentId: struct.student._id,
                    teacherId: struct.teacher._id,
                    mark: input.mark,
                    date: getNiceValidDate() + ` (Last updated by ${struct.teacher.fullname})`,
                    comment: input.comment,
                    type: input.type
                })
                    .then(data => {
                        res.redirect('/scores');
                    })
                    .catch(err => res.send(err.toString()))

        })
        .catch(err => {
          res.send("Sorry, try to reenter names of Student and teacher" + "\n\r [additional info:\n\r" + err.toString() + "]");
        });
  });

    app.post("/deleteScore/:id", (req, res) => {
        Scores.delete(req.params.id)
            .then((data) => {  res.redirect("/scores")})
            .catch(err => res.send(err.toString()));
    }); 

  app.get(
      "/delAllScores", (req, res) => { Scores.delete().then(res.send("ok")); });

}

function getUsersByNames(st_Name, teacher_Name) {
  return User.schema.methods.getAll()
      .then(users => {
        let st = -1;
        let teacher = -1;
        for (let singleUser of users) {
          if (typeof(singleUser) !== "undefined")
            if (singleUser.fullname !==
                undefined)  // should be deleted after database will renew
              if (singleUser.fullname.includes(st_Name))
                st = singleUser;
              else if (singleUser.fullname.includes(teacher_Name))
                teacher = singleUser;
        }

        if (st === -1)
          Promise.reject(
              "Sorry, can`t find student " + `${st_Name}` +
              " please, check the correctness of the entered data");
        else

        if (teacher === -1)
          Promise.reject(
              "Sorry, can`t find teacher " + `${teacher_Name}` +
              " please, check the correctness of the entered data");
        else
            return {student: st, teacher: teacher};
      })
      .catch(err => Promise.reject(err));  //`nsource: getUsersByNames\nInput
                                   //parameters: st_Name = ${st_Name}\n
                                   //teacher_Name = ${teacher_Name}`));
}


function getNiceValidDate() {
  return (new Date()).toString().substr(0, 25);
}

// route middleware to make sure a user is logged in
function isLoggedIn(req) {
  return req.isAuthenticated();
}

function isContainKeyWord(score, inputStr, student, teacher) {
  try {
    if (inputStr === undefined || score.date.toString().includes(inputStr) ||
        score.comment.toString().includes(inputStr) ||
        score.type.includes(inputStr) ||
        score.mark.toString().includes(inputStr) ||
        student.fullname.includes(inputStr) ||
        student.birthDate.toString().includes(inputStr) ||
        teacher.login.includes(inputStr) || teacher.fullname.includes(inputStr))
      return true;
  } catch (err) {
    return false;
  }
  return false;
}

function getUserById(AllUser, id) {
  for (let user of AllUser)
    if (user.id === id) return user;
  return {};
}


function getPageSize(arr, scoresPerPage) {
  return Math.ceil(arr.length / scoresPerPage);
}


function getScoresList(arr, page, scoresPerPage) {
  const res = [];
  --page;

  for (let i = page * scoresPerPage;
       i < page * scoresPerPage + scoresPerPage && i < arr.length; i++)
    res.push(arr[i]);
  return res;
}
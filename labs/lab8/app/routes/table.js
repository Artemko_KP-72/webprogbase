const fs = require('fs');
// app/routes.js
const Scores = require('../models/scores');
const Table = require('../models/table');
const User = require('../models/user');

module.exports = function(app, passport) {

app.get("/tables", (req, res) => {


    if (!isLoggedIn(req)) {
        res.redirect("/home");
        return;
      }

    Table.getAll()
        .then(tablesArray => {
            User.schema.methods.getAll()
                .then((usersArray) => {

                    let container = {   list : []   }

                    for (let i = 0; i < tablesArray.length; i++) {
                        const obj = { 
                            user: req.user,
                            linkToSingleUnit : "",
                            table : {},
                            TeacherName : "",
                            Subj: "",
                            name: ""
                        }
                        for (let singleUser of usersArray)
                            if ((singleUser._id).toString() === tablesArray[i].teacherId) { 
                                // console.log(container.list[i]);
                                // console.log(singleUser.fullname);
                                obj.TeacherName = singleUser.fullname; // adding personal information about teacher
                                obj.Subj = singleUser.login;
                            }

                        obj.name = tablesArray[i].name; // adding information about table
                        obj.table = tablesArray[i];
                        obj.linkToSingleUnit = "/table/" + tablesArray[i]._id.toString();
                        container.list.push(obj);
                    }
                    res.render("./table/tables", container);
            })
            .catch(err => res.send(err.toString()))
        })
        .catch(err => res.send(err.toString()))
    });

app.get("/table/:ID", (req, res) => {
    const input = require('url').parse(req.url,true).query;
    Table.get(req.params.ID)
        .then(el => {
            console.log("Message");
            console.log(el);
            console.log(req.params.ID);
            console.log(typeof el.teacherId);
            User.schema.methods.getById(el.teacherId)
                .then( teacher => {
                    console.log()
                    Scores.getAll()
                    .then( scoresArray => {
                            const container = {
                                dates: undefined,//getColumnsOfTable(scoresArray),
                                linkToUpdateScore: "/deleteSingleScoreFromTable/" + req.params.ID,
                                linkToDeleteScore: "/deleteSingleTable/" +  req.params.ID,
                                linkToAddScore: "/tableAddScore/" + req.params.ID,
                                TeacherName : teacher[0].fullname,
                                Subj : teacher[0].login, // todo
                                name : el.name,
                                tableBody : createComplicateStructure(el.students, scoresArray)  // why this is work
                        }
                        res.render("./table/table", container);
                    }).catch(err => res.send(err.toString()));
        }).catch(err => res.send(err.toString()));
    }).catch(err => res.send(err.toString()));
});


    app.get("/newTable", (req, res) => {

        if (req.user.role !== "1") { res.send("You have no rights to create new table"); return }

        const input = require('url').parse(req.url,true).query;
        console.log(input);
    
        User.findOne({ 'fullname' :  input.teacherName }, (err, user) => {
            // if there are any errors, return the error before anything else
            if (err) {
                console.log(console.log("Lol"));
                console.log(err.toString());
                return;
            }

            console.log(user);

        Table.init(req.user._id, input.journalName)
            .then(el => {
                console.log(el);
                res.redirect("/table/" + el._id);
            })
            .catch(err => res.send(err.toString()));

        });
    });


    app.get("/tableAddScore/:table_id", (req, res) => {  
        const input = require('url').parse(req.url,true).query;
        console.log(input);
        console.log("1");
    
        Scores.getById(input.score_id)
            .then(el => {
                console.log("2");
                User.schema.methods.getById(el.studentId)
                    .then( (st) => {

        console.log("3");
                        console.log(el);
                        console.log(st[0]);
                        Table.get(req.params.table_id)
                            .then(table => {  

        console.log("4");
                                
                                if (el.teacherId !== table.teacherId) {res.send(`Sorry, this teacher can not post mark on this discipline `); return;}
                                console.log("5");
                                console.log(!tableAllreadyHaveStudent(table, st[0]));
                                if (!tableAllreadyHaveStudent(table, st[0])) {

        console.log("6");
        console.log(st);
                                    res.redirect("/tableAddSt/" + table._id + "/" + st[0]._id); // can`t pass the student entity !!!
                                    Table.addScore(req.params.table_id, el)
                                        .then(data => {})
                                        .catch(err => {console.log("cant add score to database")});
                                } else {
                                    console.log("7");
                                    // if (el.teacherId) // author should be the leader of the course
                                    Table.addScore(req.params.table_id, el)
                                        .then(data => {
                                            console.log(data);
                                            res.redirect("/tables")
                                        })
                                        .catch(err => res.send(err.toString()));
                                }
                                })})
                                .catch(err => {console.log(err.toString()); res.send(err.toString());})
                    .catch(err => {console.log(err.toString()); res.send(err.toString());})
            })
            .catch(err => res.send(err.toString()));
    });


app.get("/tableAddSt/:table_ID/:stud_ID", (req, res) => { // /tableAddSt/5bdb60e49589142e7847f9d0
    User.schema.methods.getById(req.params.stud_ID)
        .then( (user) => {
            Table.addSt(req.params.table_ID, user[0])
                .then((st) => {console.log(st); 
                    res.redirect("/tables");
                })
                .catch(err => {console.log(err.toString());  res.send(err.toString());});
        })
        .catch(err => {console.log(err.toString());  res.send(err.toString());});
});

app.get("/delAll", (req, res) => {
    Table.delete()
    .then(res.send("ok"));   
});


app.get("/deleteSingleTable/:id", (req, res) => {
    if (req.params.id)
        Table.delete(req.params.id)
            .then((data) => {  res.redirect("/tables")})
            .catch(err => res.send(err.toString()));
    else res.redirect("/tables");
}); 

}
    // route middleware to make sure a user is logged in
    function isLoggedIn(req) {
        return req.isAuthenticated();
    }

    function getColumnsOfTable(scores) {
        const columns = [];//new Set();
        const zalupa = [];
        let col;

        for(let score of scores) {
            col = score.date.substr(0, 15);
            if (columns.find(x => x === col)); else
            if (isContainsString(columns, col) === false) 
                columns.push({date: col});
        }
        zalupa.push('gavno');
        console.log(zalupa.includes('gavno'));

        console.log(isContainsString(zalupa, 'gavno'));
        console.log(isContainsString(zalupa, 'ochko'));
        console.log(scores);

        columns.sort();
        console.log(columns);
        return columns;
    }

    function isContainsString(array, str) {
        for (let singleStr in array) 
            if (isEqualsStrings(singleStr, str))
                return true;
        return  false;
    }

    function isEqualsStrings(str1, str2) {
        console.log(typeof str1);
        if (str1.length < str2.length)
            return false;
        for (let i = 0; i < str2.length; i++)
            if (str1.charAt(i) !== str2.charAt(i))
                return false
        return true;
    }


function tableAllreadyHaveStudent(table, st) {
    for (let i of table.students) 
        if (i._id.toString() === st._id.toString())
            return true;
    return false;
}



function createComplicateStructure(studArray, scoreArray) {
    const tableBodyContainer = [];
    const complStrct = {
        self : {},
        scores: []
    }

    for (let singleStud of studArray) {
        const complStrct = {
            self : singleStud,
            scores: []
        }

        for (let singleScore of scoreArray)
            if (singleStud._id.toString() === singleScore.studentId.toString())
                complStrct.scores.push(singleScore);
        
        tableBodyContainer.push(complStrct);
    }
    return tableBodyContainer;
}

const fs = require('fs');
const path = require('path');

function readWebDoc(fileName) {
    console.log(__dirname);
    const filePath = path.join(__dirname, fileName);
    return fs.readFileSync(filePath).toString();
};

// console.log(getPredictionForUser("12"));
// let b = new Date().toString();


// function getPredictionForUser(id) {
    
//             let hash = 0; let i; let chr;
//             let str;
//             if (id === undefined) str = new Date().toString().substr(0, 16);
//             else str = id.toString() + new Date().toString().substr(0, 16); // create str
    
//             for (i = 0; i < str.length; i++) {
//               chr   = str.charCodeAt(i);
//               hash  = ((hash << 5) - hash) + chr;
//               hash |= 0; // Convert to 32bit integer
//             }
//             const database = readWebDoc("predictions.txt").split("\n");
//             return database[Math.abs(hash % database.length)];
//         }

module.exports = {
    
    // based on day of year & user id generate prediction that will change every day
    getWishForUser :(id) => {

        let hash = 0; let i; let chr;
        let str;
        if (id === undefined) str = new Date().toString().substr(0, 16);
        else str = id.toString() + new Date().toString().substr(0, 16); // create str

        for (i = 0; i < str.length; i++) {
          chr   = str.charCodeAt(i);
          hash  = ((hash << 5) - hash) + chr;
          hash |= 0; // Convert to 32bit integer
        }
        const database = readWebDoc("predictions.txt").split("\n");
        return database[Math.abs(hash % database.length)];
    }
    
    };
    
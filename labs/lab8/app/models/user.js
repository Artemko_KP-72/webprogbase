// app/models/user.js
// load the things we need
const mongoose = require('mongoose');
const Crypto = require('crypto-js');
const secretString = "%*(*~_^:!:^_~*)*%";

// define the schema for our user model
const userSchema = mongoose.Schema({

        login        : String,
        password     : String,
        role         : String,
        fullname     : String,
        email        : String,
        birthDate    : String,
        avaUrl       : String,
        selfLink     : String,
        isDisabled   : String,
});
// methods ======================
// generating a hash

const UserModel = mongoose.model('User', userSchema);

userSchema.methods.generateHash = function(password) {
    return Crypto.SHA512(password + secretString).toString();
};

// checking if password is valid
userSchema.methods.validPassword = (user, password) => {
    console.log(user.password === UserModel.schema.methods.generateHash(password));
    return user.password === UserModel.schema.methods.generateHash(password);
};

userSchema.methods.getAll = () => {
    return UserModel.find();
},

userSchema.methods.delete = (id) => {
    if (id === undefined)
        return UserModel.remove();
    return UserModel.remove( {"_id": id});
},

userSchema.methods.getById = (id) => {
    return UserModel.find({ "_id" : id.toString() });
},


userSchema.methods.update = (element, input) => {
    return UserModel.update(
                { "_id": element.id },
                { "$set":
                   {
                    "login": input.login || element.login,
                    "password": userSchema.methods.generateHash(input.password).toString() || element.password,
                    "role": input.role || element.login || '0',
                    "fullname": input.fullname || element.fullname,
                    "email": input.email || element.email,
                    "birthDate": (new Date(input.birthDate)).toString().substr(0, 25) || element.birthDate,
                    "avaUrl": input.avaUrl || element.avaUrl, // @TODO add "/images/" to pass
                    "selfLink": "/user/",
                    "isDisabled": false
                   }
                });
            },

userSchema.methods.insert = (user) => {
    console.log(user);
    return new UserModel(user).save();
}



// create the model for users and expose it to our app
module.exports = UserModel;
var XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;
const fs = require('fs');
const path = require('path');
const UrlTemplate = `https://api.teleport.org/api/cities/?search=`;

function askQuestion() {
	process.stdout.write('Input city name: ');
}

function processInput(buffer) {
	let inputString = buffer.toString().trim();
	if (inputString) {
            console.log(`Entered city: '${inputString}'`);
            const cityInfo = readFileFromData(inputString);
            if (cityInfo === "undefined") {
                httpGetAsync(UrlTemplate + inputString, (rawdata) => {
                    const result = JSON.parse(rawdata);
                    try {
                        const newUrl = result._embedded['city:search-results'][0]._links['city:item'].href;
                        console.log(newUrl);
                        httpGetAsync(newUrl, (response) => {

                            const inf = JSON.stringify(JSON.parse(response), null, 4);
                            
                            fs.writeFile("./data/" + inputString + ".json" ,inf , (err) => {
                                if(err)
                                    return console.log(err);
                                console.log("File was succesfully saved!");
                                askQuestion(); 
                            });
                            
                            console.log(inf);
                        });
                    } catch (e) {   
                        console.log(`Sorry, can not find city`);
                        askQuestion();    
                    }
                });
            }
            else  {
                console.log(cityInfo);
                askQuestion();
            }
	} else {
		// exit
		console.log(`Exit.`);
		process.stdin.end();  // stop listening input
	}
}

process.stdin.addListener('data', processInput);

askQuestion();


function readFileFromData(inputString) {
    try {
        console.log('reading ' + inputString + '.json');
        const filePath = path.join("./data/", inputString + ".json");
        return fs.readFileSync(filePath).toString();
    } catch(e) {    return typeof(undefined);   }
}

function httpGetAsync(theUrl, callback)
{   
    try {
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.onreadystatechange = function() { 
            if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
                callback(xmlHttp.responseText);
        }
        xmlHttp.open("GET", theUrl, true); // true for asynchronous 
        xmlHttp.send(null);
    } catch (e) {
        callback(e.toString());
    }
}
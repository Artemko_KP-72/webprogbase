
const express  = require('express');
const app      = express();
const port     = 8080;
const bodyParser = require('body-parser');


const mysql = require('mysql');

// First you need to create a connection to the db



app.use(bodyParser()); // get information from html forms
app.use(bodyParser.json()); // support json encoded bodies

// API
//const apiRouter = require('./app/routes/api.js')(app, passport);
//require('./app/routes/developer.js')(app, passport);

// launch ======================================================================
app.listen(port, (data) => {
    const con = mysql.createConnection({
        host: 'http://localhost:8080'
      });
      
    con.connect((err) => {
        if(err){
          console.log(err);
          return;
        }
        console.log('Connection established');
      });

      con.end((err) => {
        // The connection is terminated gracefully
        // Ensures all previously enqueued queries are still
        // before sending a COM_QUIT packet to the MySQL server.
      });
});
console.log(`Server started http://localhost:${port}`); 

app.post("/theme", function(req, res) { 
    const input = require('url').parse(req.url, true).query;
    const employee = { name: 'Winnie', location: 'Australia' };
    con.query('INSERT INTO candidates SET ?', input, (err, res) => {
      if(err) throw err;
    
      console.log('Function finished');
    });
    
    console.log(input);
    res.json(req.body);
 });

 



app.get("/:path", function(req, res) { 
    res.send(`Sorry path /"${req.params.path}/" is unavailable or does not exist`);
 });
/*
 con.query('SELECT * FROM employees', (err,rows) => {
    if(err) throw err;
  
    console.log('Data received from Db:\n');
    console.log(rows);
  });

  con.query(
    'UPDATE employees SET location = ? Where ID = ?',
    ['South Africa', 5],
    (err, result) => {
      if (err) throw err;
  
      console.log(`Changed ${result.changedRows} row(s)`);
    }
  );
  */